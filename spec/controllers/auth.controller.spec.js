const AuthController = require('../../controllers').AuthController;

describe('AuthController class', () => {
    let authController;
    let mockUserDao;
    let mockSessionDao;
    let mockRoleDao;
    let mockSecurityUtil;
    let user;

    beforeEach(() => {
        mockUserDao = jasmine.createSpyObj('UserDao',
            ['createUser', 'findUserByEmail', 'findUserByEmailAndPassword', 'findUserById', 'addRole']);
        mockSessionDao = jasmine.createSpyObj('SessionDao',
            ['createSession', 'findSessionByUserId', 'findSessionsByUser', 'setUser', 'destroySessionBySessionsId']);
        mockRoleDao = jasmine.createSpyObj('RoleDao', ['findRoleByName']);
        mockSecurityUtil = jasmine.createSpyObj('SecurityUtil', ['hashPassword', 'randomToken']);
        authController = new AuthController(mockUserDao, mockSessionDao, mockRoleDao, mockSecurityUtil);

        // set mock methods
        mockSecurityUtil.hashPassword.and.returnValue('totoPasswordHash');

        // user data
        user = {login: 'toto', email: 'toto@toto.toto', password: 'totoPassword'};
    });

    describe('method subscribe', () => {

        it('should create user', async () => {

            mockUserDao.createUser.and.returnValue(user);

            const result = await authController.subscribe(user.login, user.email, user.password);
            user.password = 'totoPasswordHash';

            expect(mockUserDao.createUser).toHaveBeenCalledTimes(1);
            expect(mockUserDao.createUser).toHaveBeenCalledWith(user.login, user.email, user.password);
            expect(result).toEqual(user);
        });

        it('should add customer role to new user', async() => {
            mockUserDao.createUser.and.returnValue(user);
            mockRoleDao.findRoleByName.and.returnValue({name: 'customer'});

            await authController.subscribe(user.login, user.email, user.password);

            expect(mockRoleDao.findRoleByName).toHaveBeenCalledTimes(1);
            expect(mockRoleDao.findRoleByName).toHaveBeenCalledWith('customer');
            expect(mockUserDao.addRole).toHaveBeenCalledTimes(1);
            expect(mockUserDao.addRole).toHaveBeenCalledWith(user, {name: 'customer'});
        });

        it('should not create user if it\'s already exists', async () => {
            mockUserDao.findUserByEmail.and.returnValue(user);

            const result = await authController.subscribe(user.login, user.email, user.password);

            expect(mockUserDao.findUserByEmail).toHaveBeenCalledTimes(1);
            expect(mockUserDao.findUserByEmail).toHaveBeenCalledWith(user.email);
            expect(mockUserDao.createUser).not.toHaveBeenCalled();
            expect(result).toBe(null);
        });
    });

    describe('method login', () => {
        let currentSession;
        const token = '54rg5egr5ererf6e4';
        beforeEach(() => {
            mockUserDao.findUserByEmailAndPassword.and.returnValue(user);
            currentSession = {
                setUser: () => {

                }
            };
            spyOn(currentSession, 'setUser');
            mockSessionDao.createSession.and.returnValue(currentSession);
            mockSecurityUtil.randomToken.and.returnValue(token);
            mockSessionDao.findSessionByUserId.and.returnValue(undefined);
        });

        it('should check if user is subscribed', async () => {
            await authController.login(user.email, user.password);

            expect(mockUserDao.findUserByEmailAndPassword).toHaveBeenCalledTimes(1);
            expect(mockUserDao.findUserByEmailAndPassword).toHaveBeenCalledWith(user.email, 'totoPasswordHash');
        });

        it('should not login if user is not subscribe', async () => {
            mockUserDao.findUserByEmailAndPassword.and.returnValue(undefined);
            const result = await authController.login(user.email, user.password);

            expect(result).toBe(null);
        });

        it('should login by create session with token', async () => {
            await authController.login(user.email, user.password);

            expect(mockSecurityUtil.randomToken).toHaveBeenCalledTimes(1);
            expect(mockSessionDao.createSession).toHaveBeenCalledTimes(1);
            expect(mockSessionDao.createSession).toHaveBeenCalledWith(token);
        });

        it('should set session to user and return the session', async () => {
            const result = await authController.login(user.email, user.password);

            expect(mockSessionDao.setUser).toHaveBeenCalledTimes(1);
            expect(mockSessionDao.setUser).toHaveBeenCalledWith(currentSession, user);
            expect(result).toBe(currentSession);
        });

        it('should throw error when user have already login and destroy session', async () => {
            mockSessionDao.findSessionByUserId.and.returnValue(currentSession);

            try {
                await authController.login(user.email, user.password);
            } catch (err) {
                expect(err).toEqual(new Error('user have already one session'));
                expect(mockSessionDao.destroySessionBySessionsId).toHaveBeenCalledTimes(1);
                expect(mockSessionDao.destroySessionBySessionsId).toHaveBeenCalledWith(user.id);
            }
        });
    });

    describe('method logout', () => {
        let sessions;
        beforeEach(() => {
            mockUserDao.findUserById.and.returnValue(user);
            sessions = [
                {id: 9, token: "65ze1r5ge1zg"},
                {id: 8, token: "65ze1r5ge1zg"}
            ];
            user.getSessions = async () => {
            };
            spyOn(user, 'getSessions');
        });
        it('must throw error if user is not found by id', async () => {
            mockUserDao.findUserById.and.returnValue(undefined);
            try {
                await authController.logout(5);
            } catch (err) {
                expect(err).toEqual(new Error('user undefined'));
            }
        });

        it('must throw error if request to get session return undefined', async () => {
            mockSessionDao.findSessionsByUser.and.returnValue(undefined);

            try {
                await authController.logout(9);
            } catch (err) {
                expect(mockSessionDao.findSessionsByUser).toHaveBeenCalledTimes(1);
                expect(err).toEqual(new Error('sessions undefined'));
            }
        });

        it('should not destroy sessions when user don\'t have session', async () => {
            mockSessionDao.findSessionsByUser.and.returnValue([]);
            const result = await authController.logout(9);

            expect(mockSessionDao.findSessionsByUser).toHaveBeenCalledTimes(1);
            expect(mockSessionDao.destroySessionBySessionsId).not.toHaveBeenCalled();
            expect(result).toBe(false);
        });

        it('should destroy sessions and return true', async () => {
            mockSessionDao.findSessionsByUser.and.returnValue(sessions);
            const result = await authController.logout(9);
            const sessionsId = sessions.map(session => session.id);

            expect(mockSessionDao.destroySessionBySessionsId).toHaveBeenCalledTimes(1);
            expect(mockSessionDao.destroySessionBySessionsId).toHaveBeenCalledWith(sessionsId);
            expect(result).toBe(true);
        });
    });
});
