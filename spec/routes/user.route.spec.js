const request = require('supertest');
const SecurityUtil = require('../../utils').SecurityUtil;
const dao = require('../../dao');
const UserDao = dao.UserDao;
const RoleDao = dao.RoleDao;
const SessionDao = dao.SessionDao;

describe('userRoute', () => {
    let server;
    let subscriber;
    let session;
    let adminMan;
    let adminSession;

    const usersToCreate = [
        {login: 'toto', password: 'totoPassword', email: 'toto@toto.toto'},
        {login: 'tata', password: 'tataPassword', email: 'tata@tata.aol'},
        {login: 'john', password: 'doe', email: 'tarte@tin.bon'}
    ];
    const users = [];

    beforeAll(async () => {
        server = await require('../../index');
        await createUsers();
        subscriber = await UserDao.findUserByEmail(users[0].email);

        const token = await SecurityUtil.randomToken();
        session = await SessionDao.createSession(token);
        await SessionDao.setUser(session, subscriber);

        // prepare admin user and session
        adminMan = await UserDao.createUser(
            'adminMan',
            'admin@admin.man',
            'adminManPwd'
        );
        const adminRole = await RoleDao.findRoleByName('admin');
        adminSession = await SessionDao.createSession('adminToken');
        await UserDao.addRole(adminMan, adminRole);
        await SessionDao.setUser(adminSession, adminMan);
    });

    async function createUsers() {
        for (let i = 0; i < usersToCreate.length; i++) {
            const user = await UserDao.createUser(
                usersToCreate[i].login,
                usersToCreate[i].email,
                usersToCreate[i].password
            );
            users.push(user.dataValues);
        }
    }

    afterAll(async () => {
        await SessionDao.destroySessionById(session.id, true);

        await SessionDao.destroySessionById(adminSession.id, true);
        await UserDao.destroyUserById(adminMan.id, true);

        await destroyUsers();
        await server.close();
    });

    async function destroyUsers() {
        for (let i = 0; i < users.length; i++) {
            await UserDao.destroyUserById(users[i].id, true);
        }
    }

    describe('request get /users', () => {
        it('should return request status 403', async () => {
            await request(server).get('/users')
                .set({"authorization": 'Bearer nonValidToken'})
                .expect(403);
        });
        it('should return request status 403 when auth is not admin', async () => {
            await request(server).get('/users')
                .set({'authorization': `Bearer ${session.token}`})
                .expect(403);
        });
        it('have to return all users', async () => {
            await request(server).get('/users')
                .set({'authorization': `Bearer ${adminSession.token}`})
                .then(response => {
                    if (response.body.length > 0) {
                        response.body.forEach(element => delete element.deletedAt);
                    }
                    expect(response.status).toEqual(200);
                    users.forEach((user, index) => {
                        const element = response.body.find(element => element.id === user.id);
                        expect(element.login).toEqual(user.login);
                        expect(element.email).toEqual(user.email);
                    })
                });
        });
        it('not have to return user\'s password', async () => {
            await request(server).get('/users')
                .set({'authorization': `Bearer ${adminSession.token}`})
                .then(response => {
                    if (response.body.length > 0) {
                        response.body.forEach(element => delete element.deletedAt);
                    }
                    expect(response.status).toEqual(200);
                    users.forEach((user, index) => {
                        const element = response.body.find(element => element.id === user.id);
                        expect(element.password).toEqual(undefined);
                    })
                });
        });
    });

    describe('request get /users/:id', () => {
        let user;
        beforeEach(() => {
            user = Object.assign({}, users[1]);
        });

        it('should return 403 when user is not admin', async () => {
            await request(server).get(`/users/${user.id}`)
        });
        it('should return 406 when id is not integer', async () => {
            await request(server).get(`/users/notnumber`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .expect(406);
            await request(server).get(`/users/1.5`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .expect(406);
        });
        it('should return 404 when id not correspond to one user id', async () => {
            await request(server).get('/users/-1')
                .set({'authorization': `Bearer ${adminSession.token}`})
                .expect(404);
        });
        it('should return 200 when id correspond to user id', async () => {
            await request(server).get(`/users/${user.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .then(response => {
                    delete response.body.deletedAt;
                    expect(response.status).toEqual(200);
                    expect(response.body.id).toEqual(user.id);
                    expect(response.body.login).toEqual(user.login);
                    expect(response.body.email).toEqual(user.email);
                    expect(response.body.createdAt.slice(0, -5)).toEqual(user.createdAt.toJSON().slice(0, -5));
                    expect(response.body.updatedAt.slice(0, -5)).toEqual(user.updatedAt.toJSON().slice(0, -5));
                });
        });
        it('should not return user\'s password', async () => {
            await request(server).get(`/users/${user.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .then(response => {
                    delete response.body.deletedAt;
                    expect(response.body.password).toEqual(undefined);
                });
        });
    });

    describe('request put /users/:id', () => {
        afterEach(async () => {
            await UserDao.updateUser(subscriber.id, subscriber.login, subscriber.email, subscriber.password);
        });

        it('should return response status 403 when auth is not admin', async () => {
            await request(server).put(`/users/${users[2].id}`)
                .set({'authorization': `Bearer ${session.token}`})
                .send({login: users[2].login, email: users[2].email, password: users[2].password})
                .set('Accept', 'application/json')
                .expect(403);
        });
        it('should return response status 406 when id is not number', async () => {
            await request(server).put('/users/oizjefoijzef')
                .set({'authorization': `Bearer ${adminSession.token}`})
                .send({login: users[2].login, email: users[2].email, password: users[2].password})
                .set('Accept', 'application/json')
                .expect(406);
        });
        it('should return response status 406 when request no have concerned body data', async () => {
            await request(server).put(`/users/${subscriber.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .set('Accept', 'application/json')
                .expect(406);
        });
        it('should return response status 406 when send only email is not correct email', async () => {
            await request(server).put(`/users/${subscriber.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .send({email: "notCorrectEmailForm"})
                .set('Accept', 'application/json')
                .expect(406);
        });
        it('should not update email when send data with not correct email', async () => {
            await request(server).put(`/users/${subscriber.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .send({login: "newLogin", email: "noijerivsz,ormjger", password: "newPassword"})
                .set('Accept', 'application/json')
                .expect(406);
        });
        it('should return response status 406 when send empty login', async () => {
            await request(server).put(`/users/${subscriber.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .send({login: ""})
                .set('Accept', 'application/json')
                .expect(406);
        });
        it('should return response status 406 when send empty password', async () => {
            await request(server).put(`/users/${subscriber.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .send({password: ""})
                .set('Accept', 'application/json')
                .expect(406);
        });
        it('should return response status 404 when user id don\'t correspond to user in database', async() => {
           await request(server).put('/users/-1')
               .set({'authorization': `Bearer ${adminSession.token}`})
               .send({login: users[2].login, email: users[2].email, password: users[2].password})
               .set('Accept', 'application/json')
               .expect(404);
        });
        it('should return response status 200 when subscriber is updated', async () => {
            await request(server).put(`/users/${subscriber.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .send({login: "newLogin", email: "newemail@email.email", password: "newPassword"})
                .set('Accept', 'application/json')
                .expect(200)
                .then(response => {
                    expect(response.body.id).toEqual(subscriber.id);
                    expect(response.body.login).toEqual("newLogin");
                    expect(response.body.email).toEqual("newemail@email.email");
                    expect(response.body.password).toEqual(SecurityUtil.hashPassword("newPassword"));
                });
        });
        it('should return response status 200 and update by sending only login', async () => {
            await request(server).put(`/users/${subscriber.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .send({login: 'newUpdatedLogin'})
                .set('Accept', 'application/json')
                .expect(200)
                .then(response => {
                    expect(response.body.id).toEqual(subscriber.id);
                    expect(response.body.login).toEqual("newUpdatedLogin");
                    expect(response.body.email).toEqual(subscriber.email);
                    expect(response.body.password).toEqual(subscriber.password);
                });
        });
        it('should return response status 200 and update by sending only email', async () => {
            await request(server).put(`/users/${subscriber.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .send({email: 'updated@email.com'})
                .set('Accept', 'application/json')
                .expect(200)
                .then(response => {
                    expect(response.body.id).toEqual(subscriber.id);
                    expect(response.body.login).toEqual(subscriber.login);
                    expect(response.body.email).toEqual('updated@email.com');
                    expect(response.body.password).toEqual(subscriber.password);
                });
        });
        it('should return response status 200 and update by sending only password', async () => {
            await request(server).put(`/users/${subscriber.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .send({password: 'updatedPassword'})
                .set('Accept', 'application/json')
                .expect(200)
                .then(response => {
                    expect(response.body.id).toEqual(subscriber.id);
                    expect(response.body.login).toEqual(subscriber.login);
                    expect(response.body.email).toEqual(subscriber.email);
                    expect(response.body.password).toEqual(SecurityUtil.hashPassword("updatedPassword"));
                });
        });
        it('should not update login when send data with empty login', async () => {
            await request(server).put(`/users/${subscriber.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .send({login: "    ", email: "newemail@email.email", password: "newPassword"})
                .set('Accept', 'application/json')
                .expect(406);
        });
        it('should not update password when send data with empty login', async () => {
            await request(server).put(`/users/${subscriber.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .send({login: "newLogin", email: "newemail@email.email", password: "        "})
                .set('Accept', 'application/json')
                .expect(406);
        });
    });

    describe('request delete /users/:id', () => {
        it('should return response status 403 when auth is not admin', async () => {
            await request(server).delete(`/users/77`)
                .set({"authorization": `Bearer ${session.token}`})
                .expect(403);
        });
        it('should return response status 400 if param is not number', async () => {
            await request(server).delete(`/users/notid`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .expect(406);
        });
        it('should return response status 404 if param id not correspond of user id', async () => {
            await request(server).delete(`/users/-1`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .expect(404);
        });
        it('should return response status 204 if delete user is success', async () => {
            let user = await UserDao.createUser('testUser', 'test@email.com', 'testPoto');

            await request(server).delete(`/users/${user.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .expect(204);

            await UserDao.destroyUserById(user.id, true);
        });
    });

    describe('request link between users and roles', () => {
        let testRole;
        let testUser;
        beforeEach(async () => {
            testUser = await UserDao.createUser('newUser', 'new@user.test', 'newUserPwd');
            testRole = await RoleDao.createRole("newRole");
            await UserDao.addRole(testUser, testRole);
        });
        afterEach(async () => {
            await UserDao.destroyUserById(testUser.id, true);
            await RoleDao.destroyRoleById(testRole.id, true);
        });

        describe('request post /users/:userId/roles/:roleId', () => {
            it('should return 403 when subscriber is not admin', async () => {
                await request(server).post(`/users/${testUser.id}/roles/${testRole.id}`)
                    .set({'authorization': `Bearer ${session.token}`})
                    .expect(403);
            });
            it('should return 406 when idUser or idRoles are not a number', async () => {
                await request(server).post('/users/notnum/roles/14')
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(406);
                await request(server).post('/users/4/roles/notnum')
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(406);
            });
            it('should return 404 with appropriate message when id user is not in database', async () => {
                await request(server).post(`/users/-7/roles/${testRole.id}`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(404)
                    .then(response => {
                        expect(response.text).toBe("User not found");
                    });
            });
            it('should return 404 with appropriate message when id role is not in database', async () => {
                await request(server).post(`/users/${testUser.id}/roles/-8`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(404)
                    .then(response => {
                        expect(response.text).toBe("Role not found");
                    });
            });
            it('should return 201 when user new role is added', async () => {
                await request(server).post(`/users/${testUser.id}/roles/${testRole.id}`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(201);

                const result = await UserDao.getUserRoles(testUser, true);
                expect(result).not.toEqual([]);
                expect(result[0].id).toEqual(testRole.id);
                await UserDao.removeUserRole(testUser, testRole, false);
            });
            it('should return 201 when user new roles are added', async () => {
                const newRole2 = await RoleDao.createRole("newRole2Test");
                await request(server).post(`/users/${testUser.id}/roles/${testRole.id}`)
                    .set({'authorization': `Bearer ${adminSession.token}`});

                await request(server).post(`/users/${testUser.id}/roles/${newRole2.id}`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(201);

                const result = await UserDao.getUserRoles(testUser, true);
                expect(result).not.toEqual([]);
                expect(result.length).toEqual(2);
                await UserDao.removeUserRole(testUser, testRole, false);
                await RoleDao.destroyRoleById(newRole2.id, true);
            });
        });
        describe('request get /users/:userId/roles', () => {
            it('should return 403 when subscriber is not admin', async () => {
                await request(server).get(`/users/${testUser.id}/roles`)
                    .set({'authorization': `Bearer ${session.token}`})
                    .expect(403);
            });
            it('should return 406 when user id is not number', async () => {
                await request(server).get(`/users/noo/roles`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(406);
            });
            it('should return 404 when user id is not number', async () => {
                await request(server).get(`/users/-987/roles`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(404)
            });
            it('should return 200 and return all roles of user', async () => {
                const anotherRole = await RoleDao.createRole("anotherRole");
                await UserDao.addRole(testUser, anotherRole);

                const roles = await UserDao.getUserRoles(testUser, true);

                await request(server).get(`/users/${testUser.id}/roles`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect('Content-Type', /json/)
                    .then(response => {
                        expect(response.status).toEqual(200);
                        const expectRoles = ['newRole', 'anotherRole'];
                        expectRoles.forEach(expectRoleName => {
                            const expectHave = response.body.some(actualRole => actualRole.name === expectRoleName);
                            expect(expectHave).toBe(true);
                        });
                    });

                await RoleDao.destroyRoleById(anotherRole.id, true);
            });
        });
        describe('request get /users/:userId/roles/:roleId', () => {
            it('should return 403 subscriber auth is not admin', async () => {
                await request(server).get(`/users/${testUser.id}/roles/${testRole.id}`)
                    .set({'authorization': `Bearer ${session.token}`})
                    .expect(403);
            });
            it('should return 406 when user id is not number', async () => {
                await request(server).get(`/users/nan/roles/${testRole.id}`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(406);
            });
            it('should return 406 when role id is not number', async () => {
                await request(server).get(`/users/${testUser.id}/roles/reject`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(406)
            });
            it('should return 404 when user by id is not in database', async () => {
                await request(server).get(`/users/-99/roles/${testRole.id}`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(404)
                    .then(response => {
                        expect(response.text).toBe('User not found');
                    });
            });
            it('should return 404 when role send by id is not in database', async () => {
                await request(server).get(`/users/${testUser.id}/roles/-654`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(404)
                    .then(response => {
                        expect(response.text).toBe('Role not found');
                    });
            });
            it('should return 404 when user don\'t have role of role id', async () => {
                const roleToTest = await RoleDao.createRole('roleToTest');
                await request(server).get(`/users/${testUser.id}/roles/${roleToTest.id}`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(404)
                    .then(response => {
                        expect(response.text).toBe('User don\'t have the role');
                    });

                await RoleDao.destroyRoleByName('roleToTest', true);
            });
            it('should return 200 when user have role', async () => {
                await request(server).get(`/users/${testUser.id}/roles/${testRole.id}`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(200)
                    .then(response => {
                        expect(response.body.id).toEqual(testRole.id);
                        expect(response.body.name).toEqual(testRole.name);
                    });
            });
        });
        describe('request put /users/:userId/roles/:roleId', () => {
            let roleToPut;
            beforeAll(async () => {
                roleToPut = await RoleDao.createRole('newRoleToPut');
            });
            afterAll(async () => {
                await RoleDao.destroyRoleByName('newRoleToPut', true);
            });

            it('should return 403 when auth is not admin', async () => {
                await request(server).put(`/users/${testUser.id}/roles/${testRole.id}`)
                    .set({'authorization': `Bearer ${session.token}`})
                    .set('Accept', 'application/json')
                    .send({'roleIdToPut': roleToPut.id})
                    .expect(403);
            });
            it('should return 406 when params userId is not number', async () => {
                await request(server).put(`/users/nop/roles/${testRole.id}`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .set('Accept', 'application/json')
                    .send({'roleIdToPut': roleToPut.id})
                    .expect(406);
            });
            it('should return 406 when params roleId is not number', async () => {
                await request(server).put(`/users/${testUser.id}/roles/yeaeuno`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .set('Accept', 'application/json')
                    .send({'roleIdToPut': roleToPut.id})
                    .expect(406);
            });
            it('should return 406 when body of new role id is not number', async () => {
                await request(server).put(`/users/${testUser.id}/roles/${testRole.id}`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .set('Accept', 'application/json')
                    .send({'roleIdToPut': 'notAgain'})
                    .expect(406);
            });
            it('should return 404 when userId not correspond to user in database', async () => {
                await request(server).put(`/users/-46/roles/${testRole.id}`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .set('Accept', 'application/json')
                    .send({'roleIdToPut': roleToPut.id})
                    .expect(404);
            });
            it('should return 404 when roleId in param not correspond to role in database', async () => {
                await request(server).put(`/users/${testUser.id}/roles/-44561`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .set('Accept', 'application/json')
                    .send({'roleIdToPut': roleToPut.id})
                    .expect(404);
            });
            it('should return 404 when roleId in body not correspond to role in database', async () => {
                await request(server).put(`/users/${testUser.id}/roles/${testRole.id}`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .set('Accept', 'application/json')
                    .send({'roleIdToPut': -4648})
                    .expect(404);
            });
            it('should return 204 when user role is replace to new role', async () => {
                await request(server).put(`/users/${testUser.id}/roles/${testRole.id}`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .set({'Accept': 'application/json'})
                    .send({'roleIdToPut': roleToPut.id})
                    .expect(204);
                const actualRole = await RoleDao.findRoleByRoleAndUserIds(roleToPut.id, testUser.id);
                expect(actualRole).not.toBe(null);
                expect(actualRole.name).toBe(roleToPut.name);
                const oldRole = await RoleDao.findRoleByRoleAndUserIds(testRole.id, testUser.id);
                expect(oldRole).toBe(null);

                await UserDao.replaceUserRole(testUser, roleToPut, testRole);
            });
        });
        describe('request delete /users/:userId/roles/:roleId', () => {
            let testDeleteRole;
            beforeEach(async () => {
                testDeleteRole = await RoleDao.createRole('deleteRole');
                await UserDao.addRole(testUser, testDeleteRole);
            });
            afterEach(async () => {
                await RoleDao.destroyRoleById(testDeleteRole.id, true);
            });

            it('should return 403 when user is not admin', async () => {
                await request(server).delete(`/users/${testUser.id}/roles/${testDeleteRole.id}`)
                    .set({'authorization': `Bearer ${session.token}`})
                    .expect(403);
            });
            it('should return 406 when userId is not number', async () => {
                await request(server).delete(`/users/toulouctoulouc/roles/${testDeleteRole.id}`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(406);
            });
            it('should return 406 when roleId is not number', async () => {
                await request(server).delete(`/users/${testUser.id}/roles/98zef56zef`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(406);
            });
            it('should return 404 when userId correspond to user is not in database', async () => {
                await request(server).delete(`/users/-77/roles/${testDeleteRole.id}`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(404);
            });
            it('should return 404 when roleId correspond to role is not in database', async () => {
                await request(server).delete(`/users/${testUser.id}/roles/-46`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(404);
            });
            it('should return 200 and delete role of user', async () => {
                await request(server).delete(`/users/${testUser.id}/roles/${testDeleteRole.id}`)
                    .set({'authorization': `Bearer ${adminSession.token}`})
                    .expect(204);

                const checkUserRole = await RoleDao.findRoleByRoleAndUserIds(testDeleteRole.id, testUser.id);
                expect(checkUserRole).toEqual(null);
            });
        });
    });
});
