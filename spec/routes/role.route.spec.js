const request = require('supertest');
const dao = require('../../dao');
const RoleDao = dao.RoleDao;
const UserDao = dao.UserDao;
const SessionDao = dao.SessionDao;
const SecurityUtil = require('../../utils').SecurityUtil;

describe('roleRoute', () => {
    let server;
    let roles;
    let subscriber;
    let subscriberSession;
    let admin;
    let adminRole;
    let adminSession;

    beforeAll(async () => {
        server = await require('../../index');
        // create normal user
        subscriber = await UserDao.createUser('subscriber', 'subscriber@toto.test', 'subscriberPassword');
        const token = await SecurityUtil.randomToken();
        subscriberSession = await SessionDao.createSession(token);
        await SessionDao.setUser(subscriberSession, subscriber);

        // create admin user and add admin role
        admin = await UserDao.createUser('admin', 'admin@test.role', 'adminPassword');
        adminRole = await RoleDao.findRoleByName('admin');
        await UserDao.addRole(admin, adminRole);
        // create admin session
        adminSession = await SessionDao.createSession(await SecurityUtil.randomToken());
        await SessionDao.setUser(adminSession, admin);

        roles = ["new tati role", "test toto role", "tesuto rolu"];

        for (let i = 0; i < roles.length; i++) {
            await RoleDao.createRole(roles[i]);
        }
    });

    afterAll(async () => {
        for (let i = 0; i < roles.length; i++) {
            await RoleDao.destroyRoleByName(roles[i], true);
        }
        await SessionDao.destroySessionById(adminSession.id, true);
        await UserDao.destroyUserById(admin.id, true);

        await SessionDao.destroySessionById(subscriberSession.id, true);
        await UserDao.destroyUserById(subscriber.id, true);

        await server.close();
    });

    describe('method post /roles', () => {
        it('should return 401 when no auth', async () => {
            await request(server).post('/roles')
                .send({name: 'test creation role'})
                .set('Accept', 'application/json')
                .then(async response => {
                    expect(response.status).toEqual(401);
                    if (response.body.length > 0) {
                        await RoleDao.destroyRoleById(response.body.id, true);
                    }
                });
        });
        it('should return 403 when auth is not admin', async () => {
            await request(server).post('/roles')
                .send({name: 'test creation role'})
                .set('Accept', 'application/json')
                .set({'authorization': `Bearer ${subscriberSession.token}`})
                .then(async response => {
                    expect(response.status).toEqual(403);
                    if (response.body.length > 0) {
                        await RoleDao.destroyRoleById(response.body.id, true);
                    }
                });
        });
        it('should return 406 when name is not send', async () => {
            await request(server).post('/roles')
                .set('Accept', 'application/json')
                .set({'authorization': `Bearer ${adminSession.token}`})
                .expect(406);
        });
        it('should return 406 when name exceed max length', async () => {
            const exceedRoleName = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            const newRole = await request(server).post('/roles')
                .send({name: exceedRoleName})
                .set({'authorization': `Bearer ${adminSession.token}`})
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(406)
                .then(response => {
                    return response.body;
                });

            if (newRole && newRole.id) {
                await RoleDao.destroyRoleById(newRole.id, true);
            }
        });
        it('should return 403 if role already exist', async () => {
            await request(server).post('/roles')
                .send({name: roles[1]})
                .set({'authorization': `Bearer ${adminSession.token}`})
                .set('Accept', 'application/json')
                .expect(403);
        });
        it('should return 201 when creation role is success', async () => {
            const newRole = await request(server).post('/roles')
                .send({name: 'test creation role'})
                .set({'authorization': `Bearer ${adminSession.token}`})
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(201)
                .then(response => {
                    expect(response.body.name).toEqual('test creation role');
                    return response.body;
                });

            if (newRole) {
                await RoleDao.destroyRoleById(newRole.id, true);
            }
        });
    });

    describe('method get /roles', () => {

        it('should return 401 when no auth', async () => {
            await request(server).get('/roles')
                .send({name: 'test creation role'})
                .set('Accept', 'application/json')
                .expect(401);
        });
        it('should return 403 when auth is not admin', async () => {
            await request(server).get('/roles')
                .set({'authorization': `Bearer ${subscriberSession.token}`})
                .expect(403);
        });
        it('should return 200 and send all existing roles', async () => {
            const result = await request(server).get('/roles')
                .set({'authorization': `Bearer ${adminSession.token}`})
                .expect('Content-Type', /json/)
                .expect(200)
                .then(response => {
                    return response.body;
                });

            roles.forEach(roleName => {
                const isRole = result.some(role => role.name === roleName);
                expect(isRole).toBe(true);
            });
        });
    });

    describe('method get /roles/:id', () => {
        let role;
        beforeAll(async () => {
            role = await RoleDao.createRole('test get role');
        });
        afterAll(async () => {
            await RoleDao.destroyRoleById(role.id, true);
        });
        it('should return 401 when no auth', async () => {
            await request(server).get(`/roles/${role.id}`)
                .send({name: 'test creation role'})
                .set('Accept', 'application/json')
                .expect(401);
        });
        it('should return 403 when auth is not admin', async () => {
            await request(server).get(`/roles/${role.id}`)
                .set({'authorization': `Bearer ${subscriberSession.token}`})
                .expect(403);
        });
        it('should return 406 when id params is not number', async () => {
            await request(server).get('/roles/toto')
                .set({'authorization': `Bearer ${adminSession.token}`})
                .expect(406);
        });
        it('should return 404 when role id is not save on database', async () => {
            const result = await request(server).get('/roles/-1')
                .set({'authorization': `Bearer ${adminSession.token}`})
                .expect(404)
                .then(response => response.body);

            expect(result).toEqual({});
        });
        it('should return 200 with role when success', async () => {
            const result = await request(server).get(`/roles/${role.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .expect('Content-Type', /json/)
                .expect(200)
                .then(response => response.body);

            expect(result.id).toEqual(role.id);
            expect(result.name).toEqual(role.name);
        });
    });

    describe('method put role/:id', () => {
        let role;
        beforeAll(async () => {
            role = await RoleDao.createRole('test get role');
        });
        afterAll(async () => {
            await RoleDao.destroyRoleById(role.id, true);
        });
        it('should return 401 when no auth', async () => {
            await request(server).put(`/roles/${role.id}`)
                .send({name: 'test creation role'})
                .set('Accept', 'application/json')
                .expect(401);
        });
        it('should return 403 when auth is not admin', async () => {
            await request(server).put(`/roles/${role.id}`)
                .set({'authorization': `Bearer ${subscriberSession.token}`})
                .expect(403);
        });
        it('should return 406 when param is not number', async () => {
            await request(server).put('/roles/notnum')
                .set({'authorization': `Bearer ${adminSession.token}`})
                .expect(406);
        });
        it('should return 406 when body is not send', async () => {
            const role = await RoleDao.createRole('test put role');
            await request(server).put(`/roles/${role.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .set('Accept', 'application/json')
                .expect(406);

            await RoleDao.destroyRoleById(role.id, true);
        });
        it('should return 404 when role id is not in database', async () => {
            await request(server).put('/roles/0')
                .send({name: 'new test put role'})
                .set({'authorization': `Bearer ${adminSession.token}`})
                .set('Accept', 'application/json')
                .expect(404);
        });
        it('should return 204 when update role is success', async () => {
            await request(server).put(`/roles/${role.id}`)
                .send({name: 'new test put role'})
                .set({'authorization': `Bearer ${adminSession.token}`})
                .set('Accept', 'application/json')
                .expect(204);
            const newRole = await RoleDao.findRoleById(role.id);

            expect(newRole.name).toEqual('new test put role');
        });
    });

    describe('method delete role/:id', () => {
        let role;
        beforeAll(async () => {
            role = await RoleDao.createRole('test get role');
        });
        afterAll(async () => {
            await RoleDao.destroyRoleById(role.id, true);
        });
        it('should return 401 when no auth', async () => {
            await request(server).delete(`/roles/${role.id}`)
                .set('Accept', 'application/json')
                .expect(401);
        });
        it('should return 403 when auth is not admin', async () => {
            await request(server).delete(`/roles/${role.id}`)
                .set({'authorization': `Bearer ${subscriberSession.token}`})
                .expect(403);
        });
        it('should return 406 when id is not number', async () => {
            await request(server).delete('/roles/nop')
                .set({'authorization': `Bearer ${adminSession.token}`})
                .expect(406);
        });
        it('should return 404 when id role is not in database', async () => {
            await request(server).delete('/roles/-666')
                .set({'authorization': `Bearer ${adminSession.token}`})
                .expect(404);
        });
        it('should return 204 when role id is deleted', async () => {
            await request(server).delete(`/roles/${role.id}`)
                .set({'authorization': `Bearer ${adminSession.token}`})
                .expect(204);
        });
    });
});
