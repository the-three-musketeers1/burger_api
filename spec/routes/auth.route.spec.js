const request = require('supertest');
const SecurityUtil = require('../../utils').SecurityUtil;
const dao = require('../../dao');
const UserDao = dao.UserDao;
const SessionDao = dao.SessionDao;

describe('authRoute', () => {
    let server;
    const subscriber = {
        login: 'john',
        password: 'lennonPassword',
        email: 'johnlennon@beatles.com'
    };
    let user;

    beforeAll(async () => {
        server = await require('../../index');
        // create one session of subscriber
        user = await request(server).post('/auth/subscribe')
            .send({login: subscriber.login, password: subscriber.password, email: subscriber.email})
            .set('Accept', 'application/json')
            .then(response => {
                return response.body;
            });
    });

    afterAll(async () => {
        await UserDao.destroyUserById(user.id, true);
        await server.close();
    });

    describe('method post /auth/subscribe', () => {
        it('should respond 406 when req don\'t have login and password and email', (done) => {
            request(server).post('/auth/subscribe').expect(406, done);
        });
        it(`should respond 406 when login is empty of have spaces`, async () => {
            for (const wrongLogin of [undefined, null, '', 'toto ', ' toto', 'toto  ']) {
                const userId = await request(server).post('/auth/subscribe')
                    .send({login: wrongLogin, password: 'totoPassword', email: 'toto@toto.toto'})
                    .set('Accept', 'application/json')
                    .expect('Content-Type', /json/)
                    .then(response => {
                        expect(response.status).toBe(406);
                        return response.body.id;
                    });
                if (userId) {
                    await UserDao.destroyUserById(userId, true);
                }
            }
        });
        it(`should respond 406 when password value is empty or have spaces`, async () => {
            for (const emptyPassword of ['', null, undefined, ' totoPassword', 'totoPassword ', ' totoPassword    ']) {
                const userId = await request(server).post('/auth/subscribe')
                    .send({login: 'toto', password: emptyPassword, email: 'toto@toto.toto'})
                    .set('Accept', 'application/json')
                    .expect('Content-Type', /json/)
                    .then(response => {
                        expect(response.status).toBe(406);
                        return response.body.id;
                    });
                if (userId) {
                    await UserDao.destroyUserById(userId, true);
                }
            }
        });
        it('should respond 406 when email property is not email', async () => {
            const userId = await request(server).post('/auth/subscribe')
                .send({login: 'toto', password: 'totoPassword', email: 'totoNoEmail'})
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .then(response => {
                    expect(response.status).toBe(406);
                    return response.body.id;
                });
            if (userId) {
                await UserDao.destroyUserById(userId, true);
            }
        });
        it('should respond 406 when email property have space', async () => {
            for (const wrongEmail of ['toto@toto.toto ', ' toto@toto.toto', 'toto@toto.toto     ']) {
                const userId = await request(server).post('/auth/subscribe')
                    .send({login: 'toto', password: 'totoPassword', email: wrongEmail})
                    .set('Accept', 'application/json')
                    .expect('Content-Type', /json/)
                    .then(response => {
                        expect(response.status).toBe(406);
                        return response.body.id;
                    });
                if (userId !== undefined) {
                    await UserDao.destroyUserById(userId, true);
                }
            }
        });
        ['login', 'password', 'email']
            .forEach(property => {
                it(`should respond 406 when ${property} max length is exceed`, async () => {
                    const data = {login: 'toto', password: 'totoPassword', email: 'toto@toto.fr'};
                    data[property] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                   const userId = await request(server) .post('/auth/subscribe')
                       .send(data)
                       .set('Accept', 'application/json')
                       .then(response => {
                           expect(response.status).toBe(406);
                           return response.body.id;
                       });
                    if (userId) {
                        await UserDao.destroyUserById(userId, true);
                    }
                });
            });
        it('should respond status 201 and return user data when req have login, password and email', async () => {
            const userId = await request(server).post('/auth/subscribe')
                .send({login: 'toto', password: 'totoPassword', email: 'toto@toto.toto'})
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(201)
                .then(response => {
                    expect(response.body.login).toEqual('toto');
                    expect(response.body.password).toEqual(SecurityUtil.hashPassword('totoPassword'));
                    expect(response.body.email).toEqual('toto@toto.toto');
                    return response.body.id;
                });
            await UserDao.destroyUserById(userId, true);
        });
        it('should respond status 403 if user is already created', async () => {
            const response = await request(server).post('/auth/subscribe')
                .send({login: 'tata', password: 'tataPassword', email: 'tata@tata.tata'})
                .set('Accept', 'application/json');

            const check = await request(server).post('/auth/subscribe')
                .send({login: 'tata', password: 'tataPassword', email: 'tata@tata.tata'})
                .set('Accept', 'application/json')
                .then(response => {
                    expect(response.status).toBe(403);
                });

            expect(check).toBe(undefined);
            await UserDao.destroyUserById(response.body.id, true);
        });
        it('should affect user role to customer after subscribe', async () => {
            const userId = await request(server).post('/auth/subscribe')
                .send({login: 'tata', password: 'tataPassword', email: 'tata@tata.tata'})
                .set('Accept', 'application/json')
                .then(response => {
                    return response.body.id;
                });
            const user = await UserDao.findUserById(userId);
            const userRole = await UserDao.getUserRoles(user, true);

            expect(userRole[0].name).toBe('customer');

            if (userId) {
                await UserDao.destroyUserById(userId, true);
            }

        })
    });

    describe('method post /auth/login', () => {
        it('should respond 406 when req don\'t have email and password', (done) => {
            request(server).post('/auth/login').expect(406, done);
        });
        it('should respond 406 when password exceed max length', async () => {
            const verylongpassword = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
            await request(server).post('/auth/login')
                .send({email: 'toto@toto.toto', password: verylongpassword})
                .set('Accept', 'application/json')
                .then(response => {
                    expect(response.status).toEqual(406);
                });
        });
        it('should respond 401 when login and password is not in the session', async () => {
            await request(server).post('/auth/login')
                .send({email: 'toto@toto.toto', password: 'unknown'})
                .set('Accept', 'application/json')
                .then(response => {
                    expect(response.status).toEqual(401);
                });
        });
        it('should respond 204 when login and password is in the session', async () => {
            const sessionId = await request(server).post('/auth/login')
                .send({email: subscriber.email, password: subscriber.password})
                .set('Accept', 'application/json')
                .then(response => {
                    expect(response.body).not.toBe(undefined);
                    expect(response.status).toBe(201);
                    return response.body.id;
                });
            await SessionDao.destroySessionById(sessionId, true);
        });
        it('should respond 403 when user have already session', async () => {
            const sessionId1 = await request(server).post('/auth/login')
                .send({email: subscriber.email, password: subscriber.password})
                .set('Accept', 'application/json')
                .then(response => response.body.id);
            const sessionId2 = await request(server).post('/auth/login')
                .send({email: subscriber.email, password: subscriber.password})
                .set('Accept', 'application/json')
                .then(response => {
                    expect(response.body).toEqual({});
                    expect(response.status).toBe(403);
                    return response.body.id ? response.body.id : undefined;
                });

            await SessionDao.destroySessionById(sessionId1, true);

            if (sessionId2) {
                await SessionDao.destroySessionById(sessionId2, true);
            }
        });
    });

    describe('method delete /auth/logout/:id', () => {
        it('should respond 406 when id is not number', async () => {
            await request(server).delete('/auth/logout/taratata')
                .then(response => {
                    expect(response.status).toBe(406);
                });
        });
        it('should respond 404 when id user is not in database', async () => {
            await request(server).delete('/auth/logout/-1')
                .then(response => {
                    expect(response.status).toBe(404);
                });
        });
        it('should respond 404 when don\'t have session', async () => {
            await request(server).delete(`/auth/logout${user.id}`)
                .then(response => {
                    expect(response.status).toBe(404);
                });
        });
        it('should response 204 when user have session', async () => {
            const sessionId = await request(server).post('/auth/login')
                .send({email: subscriber.email, password: subscriber.password})
                .set('Accept', 'application/json')
                .then(response => {
                    return response.body.id;
                });

            await request(server).delete(`/auth/logout/${user.id}`)
                .set('Accept', 'application/json')
                .expect(204);

            const check = await SessionDao.findSessionById(sessionId);
            expect(check).toEqual(null);

            await SessionDao.destroySessionById(sessionId, true);
        });
        it('should response 404 when user logout already', async () => {
            const sessionId = await request(server).post('/auth/login')
                .send({email: subscriber.email, password: subscriber.password})
                .set('Accept', 'application/json')
                .then(response => {
                    return response.body.id;
                });

            await request(server).delete(`/auth/logout/${user.id}`).set('Accept', 'application/json');
            await request(server).delete(`/auth/logout/${user.id}`)
                .set('Accept', 'application/json')
                .then(response => {
                    expect(response.status).toEqual(404);
                });

            await SessionDao.destroySessionById(sessionId, true);
        });
    });

    describe('method put /auth/subscribe/:id', () => {
        let sessionUser;
        beforeAll(async () => {
            const token = await SecurityUtil.randomToken();
            const userToLogin = await UserDao.findUserById(user.id);
            sessionUser = await SessionDao.createSession(token);
            await SessionDao.setUser(sessionUser, userToLogin);
        });
        afterAll(async () => {
            await SessionDao.destroySessionById(sessionUser.id, true);
        });
        it('should response 406 when id is not number', async () => {
            await request(server).put('/auth/subscribe/notNum')
                .send({login: 'changeSub', email: 'change@email.sub', password: 'changePwdSub'})
                .set({'authorization': `Bearer ${sessionUser.token}`})
                .set('Accept', 'application/json')
                .expect(406);
        });
        it('should response 406 when login is empty or have space', async () => {
            for (const wrongLogin of [undefined, null, '', 'toto ', ' toto', 'toto  ']) {
                await request(server).put(`/auth/subscribe/${user.id}`)
                    .send({login: wrongLogin, email: 'change@email.sub', password: 'changePwdSub'})
                    .set({'authorization': `Bearer ${sessionUser.token}`})
                    .set('Accept', 'application/json')
                    .expect(406);
            }
        });
        it('should response 406 when password is empty or have space', async () => {
            for (const emptyPassword of ['', null, undefined, ' totoPassword', 'totoPassword ', ' totoPassword    ']) {
                await request(server).put(`/auth/subscribe/${user.id}`)
                    .send({login: 'newLogin', email: 'change@email.sub', password: emptyPassword})
                    .set({'authorization': `Bearer ${sessionUser.token}`})
                    .set('Accept', 'application/json')
                    .expect(406);
            }
        });
        it('should response 406 when email is not valid', async () => {
            await request(server).put(`/auth/subscribe/${user.id}`)
                .send({login: 'toto', password: 'totoPassword', email: 'totoNoEmail'})
                .set({'authorization': `Bearer ${sessionUser.token}`})
                .set('Accept', 'application/json')
                .expect(406);
        });
        ['login', 'password', 'email']
            .forEach(property => {
                it (`should response 406 when ${property}`, async () => {
                    const data = {login: 'toto', password: 'totoPassword', email: 'totoNoEmail'};
                    data[property] =  "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" +
                        "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                    await request(server).put(`/auth/subscribe/${user.id}`)
                        .send({data})
                        .set({'authorization': `Bearer ${sessionUser.token}`})
                        .set('Accept', 'application/json')
                        .expect(406);
                });
            });
        it('should response 401 when auth is not login', async () => {
            await request(server).put(`/auth/subscribe/${user.id}`)
                .send({login: 'changeSub', email: 'change@email.sub', password: 'changePwdSub'})
                .set('Accept', 'application/json')
                .expect(401);
        });
        it('should response 403 when auth is not correspond to user id', async () => {
            const hacker = await UserDao.createUser('hacker', 'hacker@trytoput.auth', 'hackerTryToPut');
            const hackerSession = await SessionDao.createSession("oiazjefoiajzeofijaef");
            await SessionDao.setUser(hackerSession, hacker);

            await request(server).put(`/auth/subscribe/${user.id}`)
                .send({login: 'itsMyAccountNow', email: 'myaccount@notyours.now', password: 'imHackerAndChangeYourData'})
                .set({'authorization': `Bearer ${hackerSession.token}`})
                .set('Accept', 'application/json')
                .expect(403);

            await SessionDao.destroySessionById(hackerSession.id, true);
            await UserDao.destroyUserById(hacker.id, true);
        });
        it('should response 204 when update subscribe data success', async () => {
            await request(server).put(`/auth/subscribe/${user.id}`)
                .send({login: 'toto', password: 'totoPassword', email: 'change@email.sub'})
                .set({'authorization': `Bearer ${sessionUser.token}`})
                .set('Accept', 'application/json')
                .expect(204);
            const checkUserUpdate = await UserDao.findUserById(user.id);
            expect(checkUserUpdate.login).toBe('toto');
            expect(checkUserUpdate.password).toBe(SecurityUtil.hashPassword('totoPassword'));
            expect(checkUserUpdate.email).toBe('change@email.sub');
            await UserDao.updateUser(user.id, user.login, user.email, user.password);
        });
    });

    describe('method delete /auth/subscribe/:id', () => {
        let userToDelete;
        let sessionUserToDelete;

        it('should response 406 when id is not number', async () => {
            await request(server).delete(`/auth/subscribe/notNum`)
                .expect(406);
        });
        it('should response 401 when user is not login', async () => {
           await request(server).delete(`/auth/subscribe/${user.id}`)
               .expect(401);
        });
        it('should response 403 when auth id is not subscriber to delete', async () => {
            const hacker = await UserDao.createUser('hacker', 'hacker@trytoput.auth', 'hackerTryToPut');
            const hackerSession = await SessionDao.createSession("oiazjefoiajzeofijaef");
            await SessionDao.setUser(hackerSession, hacker);

            await request(server).delete(`/auth/subscribe/${user.id}`)
                .set({'authorization': `Bearer ${hackerSession.token}`})
                .expect(403);

            await SessionDao.destroySessionById(hackerSession.id, true);
            await UserDao.destroyUserById(hacker.id, true);
        });
        it('should response 204 when subscribe delete is successful', async () => {
            userToDelete = await UserDao.createUser(
                'userToDelete',
                'user@delete.to',
                SecurityUtil.hashPassword('pwdUserToDelete')
            );
            sessionUserToDelete = await SessionDao.createSession(await SecurityUtil.randomToken());

            await SessionDao.setUser(sessionUserToDelete, userToDelete);
            await request(server).delete(`/auth/subscribe/${userToDelete.id}`)
                .set({'authorization': `Bearer ${sessionUserToDelete.token}`})
                .expect(204);
            const checkUserDelete = await UserDao.findUserById(userToDelete.id);
            expect(checkUserDelete).toBe(null);

            // clean session and user to delete
            await SessionDao.destroySessionById(sessionUserToDelete.id, true);
            await UserDao.destroyUserById(userToDelete.id, true);
        });
    });
});
