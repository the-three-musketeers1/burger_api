const { param, body } = require('express-validator');

class RoleValidator {
    static checkNameValidationRules() {
        return body('name').exists({checkFalsy: true}).isString().isLength({max: 255});
    }

    static checkIdValidationRules() {
        return param('id').exists({checkFalsy: true}).isNumeric();
    }

    static checkIdNameValidationRules() {
        return [
            RoleValidator.checkNameValidationRules(),
            RoleValidator.checkIdValidationRules()
        ]
    }
}

module.exports = RoleValidator;
