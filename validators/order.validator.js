const { param, body, oneOf } = require('express-validator');

const createOrderRules = () => {
    return [
        oneOf([
            body('productData').exists({checkFalsy: true}),
            body('menuData').exists({checkFalsy: true})
        ]),
        body('productData.*.quantity').if(body('productData').exists({checkFalsy: true}))
            .isInt(),
        body('productData.*.productId').if(body('productData').exists({checkFalsy: true}))
            .isInt(),

        body('menuData.*.quantity').if(body('menuData').exists({checkFalsy: true}))
            .isInt(),
        body('menuData.*.menuId').if(body('menuData').exists({checkFalsy: true}))
            .isInt(),

        body('shopId').isInt()
    ];
};

const updateOrderRules =() => {
    return [
        param('id').exists({checkFalsy: true}),
        oneOf([
            body('productData').exists({checkFalsy: true}),
            body('menuData').exists({checkFalsy: true}),
            body('userId').exists({checkFalsy: true}),
            body('shopId').exists( {checkFalsy: true})
        ]),

        body('productData.*.quantity').if(body('productData').exists({checkFalsy: true}))
            .isInt(),
        body('productData.*.productId').if(body('productData').exists({checkFalsy: true}))
            .isInt(),

        body('menuData.*.quantity').if(body('menuData').exists({checkFalsy: true}))
            .isInt(),
        body('menuData.*.menuId').if(body('menuData').exists({checkFalsy: true}))
            .isInt(),

        body('resetProduct').if(body('productData').exists()).exists({checkFalsy: true})
            .if(body('resetProduct').exists({checkFalsy: true})).isBoolean(),
        body('resetMenu').if(body('menuData').exists()).exists({checkFalsy: true})
            .if(body('resetMenu').exists({checkFalsy: true})).isBoolean(),

        body('shopId').if(body('shopId').exists( {checkFalsy: true})).isInt(),
        body('userId').if(body('userId').exists( {checkFalsy: true})).isInt()

    ]

};

const checkId = () => {
    return [
        param('id').exists({checkFalsy: true})
    ]
};

module.exports = {
    createOrderRules,
    updateOrderRules,
    checkId
};
