const { param, body, oneOf } = require('express-validator');

const createMenuRules = () => {
    return [
        body('name').exists({checkFalsy: true}).isString(),
        body('productIds').isArray().notEmpty()
            .custom((value) => value.every(val => Number.isInteger(val))),
        body('shopIds').isArray().notEmpty()
            .custom((value) => value.every(val => Number.isInteger(val))),
        body('price').isDecimal()
    ];
};

const updateMenuRules = () => {
    return [
    param('id').exists({checkFalsy: true}),
        oneOf([
            body('name').exists({checkFalsy: true}).isString(),
            body('productIds').isArray().notEmpty().isInt(),
            body('shopIds').isArray().notEmpty().isInt(),
            body('price').isDecimal()
        ]),
        body('price').if(body('price').exists({checkFalsy: true})).isDecimal(),
        body('name').if(body('name').exists({checkFalsy: true})).isString(),
        body('shopIds').if(body('shopIds').exists({checkFalsy: true})).isArray().notEmpty()
                .custom((value) => value.every(val => Number.isInteger(val))),

        body('productIds').if(body('productIds').exists({checkFalsy: true})).isArray().notEmpty()
            .custom((value) => value.every(val => Number.isInteger(val))),

        body('resetProduct').if(body('productIds').exists()).exists({checkFalsy: true})
            .if(body('resetProduct').exists({checkFalsy: true})).isBoolean(),
        body('resetShop').if(body('shopIds').exists()).exists({checkFalsy: true})
            .if(body('resetShop').exists({checkFalsy: true})).isBoolean(),
    ];
};

const checkId = () => {
    return [
        param('id').exists({checkFalsy: true})
    ];
}


module.exports = {
    createMenuRules,
    updateMenuRules,
    checkId
};
