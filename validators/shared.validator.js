const {param, body} = require('express-validator');

class SharedValidator {

    /**
     * validation if body data is int
     * @param fieldName {string}
     * @returns {ValidationChain}
     */
    static isBodyInt(fieldName) {
        return body(fieldName).exists({checkFalsy: true}).isInt();
    }

    /**
     * validation if param data is int
     * @param fieldName {string}
     * @returns {ValidationChain}
     */
    static isParamInt(fieldName) {
        return param(fieldName).exists({checkFalsy: true}).isInt();
    }

    /**
     * validation if body data is number
     * @param fieldName {string}
     * @returns {ValidationChain}
     */
    static isBodyNumber(fieldName) {
        return body(fieldName).exists({checkFalsy: true}).isNumeric();
    }

    /**
     * validation if param data is number
     * @param fieldName {string}
     * @returns {ValidationChain}
     */
    static isParamNumber(fieldName) {
        return param(fieldName).exists({checkFalsy: true}).isNumeric();
    }

    /**
     * validation if body data is string
     * @param fieldName {string}
     * @returns {ValidationChain}
     */
    static isBodyString(fieldName) {
        return body(fieldName).exists({checkFalsy: true}).isString();
    }

    /**
     * validation if param data is string
     * @param fieldName {string}
     * @returns {ValidationChain}
     */
    static isParamString(fieldName) {
        return param(fieldName).exists({checkFalsy: true}).isString();
    }

    /**
     * validation if body is date
     * @param fieldName {string}
     * @returns {ValidationChain}
     */
    static isBodyDate(fieldName) {
        return body(fieldName).exists().isISO8601();
    }

    /**
     * validation if param is date
     * @param fieldName {string}
     * @returns {ValidationChain}
     */
    static isParamDate(fieldName) {
        return param(fieldName).exists().isISO8601();
    }
}

module.exports = SharedValidator;
