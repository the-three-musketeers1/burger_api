const { param, body } = require('express-validator');
const SharedValidator = require('./shared.validator');

class AuthValidator {
    static subscribeValidationRules() {
        return [
            body('login').exists({checkFalsy: true})
                .isString().isLength({max: 255})
                .custom(value => !/\s/.test(value)),
            body('email').exists({checkFalsy: true}).isEmail(),
            body('password').exists({checkFalsy: true})
                .isString()
                .isLength({max: 255})
                .custom(value => !/\s/.test(value))
        ]
    }

    static loginValidationRules() {
        return [
            SharedValidator.isBodyString('email'),
            SharedValidator.isBodyString('password').isLength({max: 255})
        ]
    }

    static logoutValidationRules() {
        return SharedValidator.isParamInt('id');
    }

    static putSubscribeValidationRules() {
        return [
            SharedValidator.isParamInt('id'),
            ...AuthValidator.subscribeValidationRules()
        ]
    }

    static deleteValidationRules() {
        return SharedValidator.isParamInt('id');
    }
}

module.exports = AuthValidator;
