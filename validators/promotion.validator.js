const { param, body } = require('express-validator');

const promotionAttributesValidationRules = () => {
    return [
        body('price').exists({ checkNull: true}).isNumeric(),
        body('in_front').exists({ checkNull: true}).isIn([0, 1]),
        body('start_date').exists().isISO8601(),
        body('end_date').exists().isISO8601()
    ];
};

const createProductPromotionValidationRules = () => {
    return [
        param('productId').exists({checkFalsy: true})
    ] && promotionAttributesValidationRules();
};

const createMenuPromotionValidationRules = () => {
    return [
        param('menuId').exists({checkFalsy: true})
    ] && promotionAttributesValidationRules();
};

const getMenuPromotionValidationRules = () => {
  return [
      param('menuId').exists({checkFalsy: true}),
      param('deadline').exists().isISO8601()
  ];
};

const getProductPromotionValidationRules = () => {
  return [
      param('productId').exists({checkFalsy: true}),
      param('deadline').exists().isISO8601()
  ];
};

const getAllPromotionsValidationRules = () => {
    return [
        param('deadline').exists().isISO8601()
    ];
};

const idPromotionRule = () => {
    return param('id').exists({checkFalsy: true});
};

const updatePromotionValidationRules = () => {
        return [
            param('id').exists({checkFalsy: true}),
            body('price').optional().isNumeric(),
            body('in_front').optional().isIn([0, 1]),
            body('start_date').optional().isISO8601(),
            body('end_date').optional().isISO8601(),
            body('menu_id').optional(),
            body('product_id').optional()
        ];
};

module.exports = {
    createProductPromotionValidationRules,
    createMenuPromotionValidationRules,
    getMenuPromotionValidationRules,
    getProductPromotionValidationRules,
    getAllPromotionsValidationRules,
    updatePromotionValidationRules,
    idPromotionRule
};