const { param, body } = require('express-validator');

const createIngredientValidationRules = () => {
    return [
        body('name').exists({checkFalsy: true}).isString()
    ];
};

const checkId = () => {
    return [
      param('id').exists({checkFalsy: true})
    ];
}

const updateIngredientValidateRules = () => {
    return [
        body('name').exists({checkFalsy: true}).isString(),
        param('id').exists({checkFalsy: true})
    ];
}

module.exports = {
    createIngredientValidationRules,
    checkId,
    updateIngredientValidateRules
};