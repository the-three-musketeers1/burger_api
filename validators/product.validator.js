const { param, body } = require('express-validator');

function checkIngredientsData(ingredientsData) {
    for(const data of ingredientsData) {
        if((!data.id && !data.IngredientId) || !data.quantity || !data.unit) {
            return false;
        }
    }

    return true;
}

function checkUpdateIngredientsData(ingredientsData) {
    for(const data of ingredientsData) {
        if(!data.id || !(data.quantity || data.unit)) {
            return false;
        }
    }

    return true;
}

const checkId = () => {
    return [
        param('id').exists({checkFalsy: true})
    ];
};

const updateProductValidationRules = () => {
  return [
      param('id').exists({checkFalsy: true}),
      body('name').optional().isString().notEmpty(),
      body('price').optional().isNumeric(),
      body('ingredientsData').optional({checkFalsy: true}).custom(val => {
          return checkUpdateIngredientsData(val);
      })

  ];
};

const createProductValidationRules = () => {
    return [
        body('name').exists({checkFalsy: true}),
        body('price').exists({checkNull: true}).isNumeric(),
        body('ingredientsData').custom(val => {
            return checkIngredientsData(val);
        })
    ];
};

const addProductIngredientValidationRules = () => {
    return [
        param('id').exists({checkFalsy: true}),
        body('ingredientsData').custom(val => {
            return checkIngredientsData(val);
        })
    ];
}

const deleteProductIngredientValidationRules = () => {
    return [
        param('productId').exists({checkFalsy: true}),
        param('ingredientId').exists({checkFalsy: true})
    ];
}


module.exports = {
    createProductValidationRules,
    updateProductValidationRules,
    checkId,
    addProductIngredientValidationRules,
    deleteProductIngredientValidationRules
};