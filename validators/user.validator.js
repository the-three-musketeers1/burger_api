const {body, oneOf} = require('express-validator');
const SharedValidator = require('./shared.validator');
const AuthValidator = require('./auth.validator');

class UserValidator {

    /**
     * Validation rules to put user
     * @returns {ValidationChain[]}
     */
    static putValidatorRules() {
        return [
            SharedValidator.isParamInt('id'),
            oneOf(AuthValidator.subscribeValidationRules()),
            body('login').optional({checkFalsy: true})
                .isString().isLength({max: 255})
                .custom(value => !/\s/.test(value)),
            body('email').optional({checkFalsy: true}).isEmail(),
            body('password').optional({checkFalsy: true})
                .isString()
                .isLength({max: 255})
                .custom(value => !/\s/.test(value))
        ];
    }

    /**
     * put user role validation rules
     * @returns {ValidationChain[]}
     */
    static putUserRoleValidationRules() {
        return [
            ...this.areParamsUserRoleIdsInt(),
            SharedValidator.isBodyInt('roleIdToPut')
        ];
    }

    /**
     * validation rules if params user id and role id is integer
     * @returns {ValidationChain[]}
     */
    static areParamsUserRoleIdsInt() {
        return [
            SharedValidator.isParamInt('userId'),
            SharedValidator.isParamInt('roleId')
        ];
    }
}

module.exports = UserValidator;
