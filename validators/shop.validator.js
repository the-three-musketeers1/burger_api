const { param, body, oneOf } = require('express-validator');


function checkIngredientsData(ingredientsData) {
    for(const data of ingredientsData) {
        if((!data.id && !data.IngredientId) || !data.quantity || !data.unit) {
            return false;
        }
    }

    return true;
}

function checkUpdateIngredientsData(ingredientsData) {
    for(const data of ingredientsData) {
        if(!data.id || !(data.quantity || data.unit)) {
            return false;
        }
    }

    return true;
}


const createShopRules = () => {
    return [
        body('name').exists({checkFalsy: true})
            .if( body('name').exists({checkFalsy: true})).isString(),
        body('address').exists({checkFalsy: true})
            .if( body('address').exists({checkFalsy: true})).isString()
    ]
};

const updateShopRules = () => {
    return [
        param('id').exists({checkFalsy: true}),
        oneOf([
            body('name').exists({checkFalsy: true}),
            body('address').exists({checkFalsy: true})
        ]),
        body('name').if(body('name').exists({checkFalsy: true})).isString(),
        body('address').if(body('address').exists({checkFalsy: true})).isString(),
        body('ingredientsData').optional({checkFalsy: true}).custom(val => {
            return checkUpdateIngredientsData(val);
        })
    ]
};

const addStockValidationRules = () => {
    return [
        param('id').exists({checkFalsy: true}),
        body('ingredientsData').custom(val => {
            return checkIngredientsData(val);
        })
    ];
}

const deleteStockRules = () => {
    return [
        param('id').exists({checkFalsy: true}),
        param('ingredientId').exists({checkFalsy: true})
    ];
}

const checkId = () => {
    return [
        param('id').exists({checkFalsy: true})
            .if(param('id').exists({checkFalsy: true})).isInt()
    ]
};


module.exports = {
    createShopRules,
    updateShopRules,
    checkId,
    addStockValidationRules,
    deleteStockRules
};
