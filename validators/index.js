module.exports = {
    PromotionValidator: require('./promotion.validator'),
    ProductValidator: require('./product.validator'),
    IngredientValidator: require('./ingredient.validator'),
    AuthValidator: require('./auth.validator'),
    MenuValidator: require('./menu.validator'),
    OrderValidator: require('./order.validator'),
    ShopValidator: require('./shop.validator'),
    RoleValidator: require('./role.validator'),
    SharedValidator: require('./shared.validator'),
    UserValidator: require('./user.validator')
};
