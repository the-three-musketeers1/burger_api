module.exports = function (sequelize, DataTypes) {
    const OrderMenu = sequelize.define('order_menus', {
        quantity: {
            type: DataTypes.INTEGER
        }
    }, {
        underscored: true
    });
    OrderMenu.removeAttribute('id');

    return OrderMenu;
}
