module.exports = function (sequelize, DataTypes) {
    const Menu = sequelize.define('Menu', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING
        },
        price: {
            type: DataTypes.FLOAT
        }
    }, {
        paranoid: true,
        freezeTableName: true,
        underscored: true,
        timestamps: true
    });

    Menu.associate = function (models) {
        Menu.belongsToMany(models.Product, { through: 'Menu_Products'});
        Menu.belongsToMany(models.Shop, {through: 'Shop_Menus'});
        Menu.belongsToMany(models.Order, {through:  models.order_menus});
        Menu.hasMany(models.Promotion)
    };

    return Menu;
};
