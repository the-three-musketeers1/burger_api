module.exports = function (sequelize, DataTypes) {
    const Ingredient = sequelize.define('Ingredient', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING
        }
    }, {
        paranoid: true,
        freezeTableName: true,
        underscored: true,
        timestamps: true
    });

    Ingredient.associate = function (models) {
        Ingredient.belongsToMany(models.Product, { through: models.Recipe});
        Ingredient.belongsToMany(models.Shop, {through: models.Stock});
    };

    return Ingredient;
};
