module.exports = function (sequelize, DataTypes) {
    return sequelize.define('Stock', {
        quantity: {
            type: DataTypes.INTEGER
        },
        unit: {
            type: DataTypes.STRING
        }
    });
};