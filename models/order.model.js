module.exports = function (sequelize, DataTypes) {
    const Order = sequelize.define('Order', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        }
    }, {
        paranoid: true,
        freezeTableName: true,
        underscored: true,
        timestamps: true
    });

    Order.associate = function (models) {
        Order.belongsTo(models.Shop);
        Order.belongsTo(models.User);
        Order.belongsToMany(models.Product,
            { through: models.order_products });
        Order.belongsToMany(models.Menu,
            {through: models.order_menus });
    };

    return Order;
};
