module.exports = function (sequelize, DataTypes) {
    const Promotion = sequelize.define('Promotion', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        price: {
            type: DataTypes.FLOAT
        },
        in_front: {
            type: DataTypes.BOOLEAN
        },
        start_date: {
            type: DataTypes.DATE
        },
        end_date: {
            type: DataTypes.DATE
        }
    }, {
        paranoid: true,
        freezeTableName: true,
        underscored: true,
        timestamps: true
    });
    Promotion.associate = function (models) {
        Promotion.belongsTo(models.Product);
        Promotion.belongsTo(models.Menu);
    };

    return Promotion;
};
