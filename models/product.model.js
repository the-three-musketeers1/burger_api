module.exports = function (sequelize, DataTypes) {
    const Product = sequelize.define('Product', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING
        },
        price: {
            type: DataTypes.FLOAT
        }
    }, {
        paranoid: true,
        freezeTableName: true,
        underscored: true,
        timestamps: true
    });

    Product.associate = function (models) {
        Product.belongsToMany(models.Ingredient, { through: models.Recipe, onDelete: 'CASCADE'});
        Product.hasMany(models.Promotion);
        Product.belongsToMany(models.Menu, { through: "Menu_Products"});
        Product.belongsToMany(models.Order, { through:  models.order_products});
    };

    return Product;
};
