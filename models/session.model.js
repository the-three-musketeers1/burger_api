module.exports = function (sequelize, DataTypes) {
    const Session = sequelize.define('Session', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        token: {
            type: DataTypes.STRING
        }
    }, {
        paranoid: true,
        freezeTableName: true,
        underscored: true,
        timestamps: true
    });
    Session.associate = function (models) {
        Session.belongsTo(models.User);
    };
    return Session;
};
