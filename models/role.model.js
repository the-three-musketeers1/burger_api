module.exports = function (sequelize, DataTypes) {
    const Role = sequelize.define('Role', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING
        }
    }, {
        paranoid: true,
        freezeTableName: true,
        underscored: true,
        timestamps: true
    });
    Role.associate = function (models) {
        Role.belongsToMany(models.User, {through: 'User_Roles'});
    };

    return Role;
};
