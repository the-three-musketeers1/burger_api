module.exports = function(sequelize, DataTypes) {
    const OrderProduct = sequelize.define('order_products', {
        quantity: {
            type: DataTypes.INTEGER
        }
    },{
        underscored: true
    });
    OrderProduct.removeAttribute('id');

    return OrderProduct;
}
