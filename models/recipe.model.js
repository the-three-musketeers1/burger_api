module.exports = function (sequelize, DataTypes) {

    return sequelize.define('Recipe', {
        quantity: {
            type: DataTypes.INTEGER
        },
        unit: {
            type: DataTypes.STRING
        }
    });
};
