module.exports = function (sequelize, DataTypes) {
    const Shop = sequelize.define('Shop', {
        id: {
            type: DataTypes.BIGINT,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: DataTypes.STRING
        },
        address: {
            type: DataTypes.STRING
        }
    }, {
        paranoid: true,
        freezeTableName: true,
        underscored: true,
        timestamps: true
    });
    Shop.associate = function (models) {
        Shop.hasMany(models.Order);
        Shop.belongsToMany(models.Menu, {through: 'Shop_Menus'});
        Shop.belongsToMany(models.Ingredient, {through: models.Stock});
    };

    return Shop;
};
