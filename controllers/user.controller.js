const dao = require('../dao');
const UserDao = dao.UserDao;
const RoleDao = dao.RoleDao;
const SecurityUtil = require('../utils').SecurityUtil;

class UserController {

    /**
     * @returns {Promise<User[]>}
     */
    static async getUsers() {
        return UserDao.findAllUsers(['password'], true);
    }

    /**
     * @param id {number}
     * @returns {Promise<User>}
     */
    static async getUser(id) {
        return UserDao.findUserById(id, ['password'], true);
    }

    /**
     * @param userId {number}
     * @param userProperties {Object}
     * @returns {Promise<User>}
     */
    static async updateUser(userId, userProperties) {
        const user = await UserDao.findUserById(userId);
        if (!user) {
            return undefined;
        }

        user.login = userProperties.login ? userProperties.login : user.login;
        user.email = userProperties.email ? userProperties.email : user.email;
        user.password = userProperties.password ? SecurityUtil.hashPassword(userProperties.password) : user.password;

        await UserDao.updateUser(user.id, user.login, user.email, user.password);

        return user;
    }

    /**
     * @param userId {number}
     * @returns {Promise<boolean>}
     */
    static async deleteUser(userId) {
        const user = await UserDao.findUserById(userId);

        if (!user) {
            return false;
        }
        await UserDao.destroyUserById(userId);
        return true;
    }

    /**
     * @param userId {number}
     * @param roleId {number}
     * @returns {Promise<UserRole>}
     */
    static async addRole(userId, roleId) {
        const user = await UserDao.findUserById(userId);
        if (!user) {
            throw new Error('User not found');
        }
        const role = await RoleDao.findRoleById(roleId);
        if (!role) {
            throw new Error('Role not found');
        }
        return UserDao.addRole(user, role);
    }

    /**
     * @param user {User}
     * @returns {Promise<Role[]>}
     */
    static async getUserRoles(user) {
        return UserDao.getUserRoles(user, true);
    }

    /**
     * @param userId {number}
     * @returns {Promise<Role[]|null>}
     */
    static async getUserRolesByUserId(userId) {
        const user = await UserDao.findUserById(userId);
        if (!user) {
            return null;
        }
        return await UserDao.getUserRoles(user);
    }

    /**
     * @param userId {number}
     * @param roleId {number}
     * @returns {Promise<Role>}
     */
    static async getUserRoleByIds(userId, roleId) {
        const user = await UserDao.findUserById(userId);
        if (!user) {
            throw new Error('User not found');
        }

        const role = await RoleDao.findRoleById(roleId);
        if (!role) {
            throw new Error('Role not found');
        }

        const userRole = await RoleDao.findRoleByRoleAndUserIds(role.id, user.id, true);

        if (!userRole) {
            throw new Error('User don\'t have the role');
        }
        return userRole;
    }

    /**
     * @param userId {number}
     * @param oldRoleId {number}
     * @param newRoleId {number}
     * @returns {Promise<boolean>}
     */
    static async putUserRole(userId, oldRoleId, newRoleId) {
        const user = await UserDao.findUserById(userId);
        if (!user) {
            return false;
        }
        const oldRole = await RoleDao.findRoleById(oldRoleId);
        if (!oldRole) {
            return false;
        }
        const newRole = await RoleDao.findRoleById(newRoleId);
        if (!newRole) {
            return false;
        }
        await UserDao.replaceUserRole(user, oldRole, newRole);
        return true;
    }

    /**
     * @param userId {number}
     * @param roleId {number}
     * @returns {Promise<boolean>}
     */
    static async deleteUserRole(userId, roleId) {
        const user = await UserDao.findUserById(userId);
        if (!user) {
            return false;
        }
        const role = await RoleDao.findRoleById(roleId);
        if (!role) {
            return false;
        }
        await UserDao.removeUserRole(user, role);
        return true;
    }
}

module.exports = UserController;
