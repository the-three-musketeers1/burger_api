const PromotionDao = require('../dao').PromotionDao;

class PromotionController {

    static isDateValid(dateToValid) {
        const date = new Date(dateToValid);
        return date.getTime() === date.getTime();
    }

    static async createMenuPromotion(menuId, price, in_front, start_date, end_date) {
        const promotion = await PromotionDao.createMenuPromotion(menuId, price, in_front, start_date, end_date);

        if(!promotion) {
            throw "Menu not found";
        }

        return promotion;
    }

    static async createProductPromotion(productId, price, in_front, start_date, end_date) {
        const promotion = await PromotionDao.createProductPromotion(productId, price, in_front, start_date, end_date);

        if(!promotion) {
            throw "Product not found";
        }

        return promotion;
    }

    static async findMenuPromotions(menuId, deadline) {
        if(this.isDateValid(deadline) === false) {
            throw "Invalid date";
        }

        return await PromotionDao.findMenuPromotions(menuId, deadline);
    }

    static async findProductPromotions(productId, deadline) {
        if(this.isDateValid(deadline) === false) {
            throw "Invalid date";
        }

        return await PromotionDao.findProductPromotions(productId, deadline);
    }

    static async findAllPromotions(deadline) {
        if(this.isDateValid(deadline) === false) {
            throw "Invalid date";
        }

        return await PromotionDao.findAllPromotions(deadline);
    }

    static async updatePromotion(id, price, in_front, start_date, end_date, product_id, menu_id) {
        await PromotionDao.updatePromotionAttributes(id, price, in_front, start_date, end_date);

        if(product_id && !await PromotionDao.isPromotionMenu(id)) {
            await PromotionDao.updatePromotionProduct(id, product_id);
        }

        if(menu_id && !await PromotionDao.isPromotionProduct(id)) {
            await PromotionDao.updatePromotionMenu(id, menu_id);
        }

        return await this.findPromotionById(id);
    }



    static findPromotionById(id) {
        return PromotionDao.findPromotionById(id);
    }

    static async deletePromotion(id) {
        return await PromotionDao.deletePromotion(id);
    }

}

module.exports = PromotionController;