const dao = require('../dao');
const ProductDao = dao.ProductDao;
const IngredientDao = dao.IngredientDao;

class ProductController {

    static async addProductIngredient(productId, ingredientsData) {
        return await ProductDao.addProductIngredient(productId, ingredientsData);
    }

    static async deleteProductIngredient(productId, ingredientId) {
        return await ProductDao.deleteProductIngredient(productId, ingredientId);
    }

    static async createProduct(name, price, ingredientsData) {
        const recipeIngredients = [];

        for (let i = 0; i < ingredientsData.length; i++) {
            const ingredient = await IngredientDao.findIngredientById(ingredientsData[i].id);
            recipeIngredients.push(ingredient);
        }

        return await ProductDao.createProduct(name, price, ingredientsData, recipeIngredients);
    }

    static async findProductById(id) {
        return await ProductDao.findProductById(id);
    }

    static async findAllProducts() {
        return await ProductDao.findAllProducts();
    }

    static async deleteProduct(id) {
        return await ProductDao.deleteProduct(id);
    }

    static async updateProduct(id, name, price, ingredientsData) {
        return await ProductDao.updateProduct(id, name, price, ingredientsData);
    }

}

module.exports = ProductController;