const RoleDao = require('../dao').RoleDao;

class RoleController {

    /**
     * @param name {string}
     * @returns {Promise<null|Role>}
     */
    static async createRole(name) {
        const role = await RoleDao.findRoleByName(name);
        if (role) {
            return null;
        }

        return RoleDao.createRole(name);
    }

    /**
     * @returns {Promise<Role[]>}
     */
    static async findAllRoles() {
        return RoleDao.findAllRoles(true);
    }

    /**
     * @param roleId {number}
     * @returns {Promise<Role|null>}
     */
    static async findRoleById(roleId) {
        return RoleDao.findRoleById(roleId);
    }

    /**
     * @param roleId {number}
     * @param name {string}
     * @returns {Promise<boolean>}
     */
    static async updateRole(roleId, name) {
        const role = await RoleDao.findRoleById(roleId);
        if (!role) {
            return false;
        }

        await RoleDao.updateRole(role.id, name);
        return true;
    }

    /**
     * @param roleId {number}
     * @returns {Promise<boolean>}
     */
    static async deleteRole(roleId) {
        const role = await RoleDao.findRoleById(roleId);

        if (!role) {
            return false;
        }

        await RoleDao.destroyRoleById(role.id);
        return true;
    }
}

module.exports = RoleController;