class AuthController {

    constructor(UserDao, SessionDao, RoleDao, SecurityUtil) {
        this.UserDao = UserDao;
        this.SessionDao = SessionDao;
        this.RoleDao = RoleDao;
        this.SecurityUtil = SecurityUtil;
    }

    /**
     * @param login {string}
     * @param email {string}
     * @param password {string}
     * @returns {Promise<User>|null}
     */
    async subscribe(login, email, password) {

        const checkUser = await this.UserDao.findUserByEmail(email);
        if (checkUser) {
            return null;
        }
        const user = await this.UserDao.createUser(login, email, this.SecurityUtil.hashPassword(password));
        const customerRole = await this.RoleDao.findRoleByName('customer');
        await this.UserDao.addRole(user, customerRole);

        return user;
    }

    /**
     * @param email {string}
     * @param password {string}
     * @returns {Promise<Session>|null}
     */
    async login(email, password) {
        const user = await this.UserDao.findUserByEmailAndPassword(email, this.SecurityUtil.hashPassword(password));
        if (!user) {
            return null;
        }

        const session = await this.SessionDao.findSessionByUserId(user.id);
        if (session) {
            await this.SessionDao.destroySessionBySessionsId(user.id);
            throw new Error('user have already one session');
        }
        const token = await this.SecurityUtil.randomToken();
        const newSession = await this.SessionDao.createSession(token);
        await this.SessionDao.setUser(newSession, user);
        return newSession;
    }

    /**
     * @param id {number}
     * @returns {Promise<boolean>}
     */
    async logout(id) {
        const user = await this.UserDao.findUserById(id);
        if (!user) {
            throw new Error('user undefined');
        }
        const sessions = await this.SessionDao.findSessionsByUser(user);
        if (!sessions) {
            throw new Error('sessions undefined');
        }
        if (sessions.length === 0) {
            return false;
        }

        const sessionsId = sessions.map(session => session.id);
        await this.SessionDao.destroySessionBySessionsId(sessionsId);
        return true;
    }

    /**
     * @param token
     * @returns {Promise<User|null>}
     */
    userFromToken(token) {
        return this.UserDao.findUserBySessionToken(token);
    }

    async updateSubscriber(subscribeId, login, email, password) {
        const subscriber = await this.UserDao.findUserById(subscribeId);
        if (!subscriber) {
            throw new Error();
        }
        subscriber.login = login ? login : subscriber.login;
        subscriber.email = email ? email : subscriber.email;
        subscriber.password = password ? this.SecurityUtil.hashPassword(password) : subscriber.password;
        await this.UserDao.updateUser(subscribeId, subscriber.login, subscriber.email, subscriber.password);
    }

    async deleteSubscribe(subscribeId) {
        const session = await this.SessionDao.findSessionByUserId(subscribeId);
        if (!session) {
            return false;
        }
        await this.SessionDao.destroySessionById(session.id);

        await this.UserDao.destroyUserById(subscribeId);
        return true;
    }
}

module.exports = AuthController;
