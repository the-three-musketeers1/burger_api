const IngredientDao = require('../dao').IngredientDao;

class IngredientController {

    static createIngredient(name) {
        return IngredientDao.createIngredient(name);
    }

    static findAllIngredients() {
        return IngredientDao.findAllIngredients();
    }

    static findIngredientById(id) {
        return IngredientDao.findIngredientById(id);
    }

    static deleteIngredient(id) {
        return IngredientDao.deleteIngredient(id);
    }

    static async updateIngredient(id, name) {
        await IngredientDao.updateIngredient(id, name);

        return await this.findIngredientById(id);
    }


}

module.exports = IngredientController;