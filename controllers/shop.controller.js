const ShopDao = require('../dao').ShopDao;
const IngredientDao = require('../dao').IngredientDao;

class ShopController {

    static createShop(name, address) {
        return ShopDao.createShop(name, address);
    }

    static async addStockToShop(id, ingredientsData) {
        const ingredients = [];

        for (const ingredientData of ingredientsData) {
            const ingredient = await IngredientDao.findIngredientById(ingredientData.id);
            ingredients.push(ingredient);
        }

        return await ShopDao.addStockToShop(id, ingredientsData, ingredients);
    }

    static async findStocksOfShop(id){
        return await ShopDao.findStocksOfShop(id);
    }

    static findAllShops() {
        return ShopDao.findAllShops();
    }

    static findShopById(id) {
        return ShopDao.findShopById(id);
    }

    static async deleteStock(id, ingredientId) {
        return await ShopDao.deleteStock(id, ingredientId);
    }

    static async deleteShop(id) {
        const shop = await this.findShopById(id);
        if (shop === null) {
            throw EmptyResultError();
        }
        return ShopDao.deleteShop(id);
    }



    static async updateShop(id, name, address, ingredientsData) {
        await ShopDao.updateShop(id, name, address, ingredientsData);

        return await this.findShopById(id);
    }


}

module.exports = ShopController;
