const ProductDao = require('../dao').ProductDao;
const MenuDao = require('../dao').MenuDao;
const UserDao = require('../dao').UserDao;
const OrderDao = require('../dao').OrderDao;
const ShopDao = require('../dao').ShopDao;
class OrderController {

    static async create(productsData, menusData, user, shopId) {
        if (productsData !== undefined) {
            for (let i = 0; i < productsData.length;i++) {
                productsData[i].product = await ProductDao.findProductById(productsData[i].productId);
            }
        } else {
            productsData = [];
        }
        if (menusData!== null ) {
            for (let i = 0; i < menusData.length;i++) {
                menusData[i].menu = await MenuDao.findById(menusData[i].menuId);
            }
        } else {
            menusData = [];
        }
        const shop = await ShopDao.findShopById(shopId);
        return OrderDao.create(productsData, menusData, user, shop);
    }

    static async update(id, productsData, menusData, userId, shopId, resetProduct, resetMenu) {
        let user = null;
        let shop = null;

        if (productsData !== undefined && menusData !== null) {
            for (let i = 0; i < productsData.length;i++) {
                productsData[i].product = await ProductDao.findProductById(productsData[i].productId);
            }
        } else {
            productsData = [];
        }

        if (menusData !== undefined && menusData !== null) {
            for (let i = 0; i < menusData.length;i++) {
                menusData[i].menu = await MenuDao.findById(menusData[i].menuId);
            }
        } else {
            menusData = [];
        }

        if (userId !== undefined && userId !== null) {
            user = await UserDao.findUserById(userId);
        }
        if (shopId !== undefined && shopId !== null) {
            shop = await ShopDao.findShopById(shopId);
        }

        return OrderDao.update(id, productsData, menusData, user, shop, resetProduct, resetMenu);
    }



    static getById(orderId) {
        return OrderDao.findById(orderId);
    }
    
    static getAll(paranoid) {
        return OrderDao.findAll(paranoid);
    }

    static getAllByShop(shopId, paranoid) {
        return OrderDao.findAllByShop(shopId, paranoid);
    }

    static getAllByUser(userId, paranoid) {
        return OrderDao.findAllByUser(userId, paranoid);
    }

    static deleteById(userId) {
        return OrderDao.delete(userId);
    }

}

module.exports = OrderController;
