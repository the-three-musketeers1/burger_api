module.exports = {
    AuthController: require('./auth.controller'),
    IngredientController: require('./ingredient.controller'),
    MenuController: require('./menu.controller'),
    OrderController: require('./order.controller'),
    ProductController: require('./product.controller'),
    PromotionController: require('./promotion.controller'),
    RoleController: require('./role.controller'),
    ShopController: require('./shop.controller'),
    UserController: require('./user.controller')
};
