

const ShopDao = require('../dao').ShopDao;
const MenuDao = require('../dao').MenuDao;
const ProductDao = require('../dao').ProductDao;
class MenuController {

    static async createMenu(name,price,productIds,shopIds) {
        const shops = await ShopDao.findAllByIds(shopIds);
        const products = await ProductDao.findAllByIds(productIds);
        return MenuDao.create(name,price,products,shops);
    }

    static async getAllByShop(shopId) {
        const shop = await ShopDao.findShopById(shopId);
        return MenuDao.findAllByShop(shop);
    }

    static getAll() {
        return MenuDao.findAll();
    }

    static getById(id) {
        return MenuDao.findById(id);
    }

    static async update(id, name, price, productIds, shopIds, resetShop, resetProduct) {
        let shops = null;
        let products = null;
        if (productIds !== undefined && productIds !== null) {
            products = await ProductDao.findAllByIds(productIds);
        }
        if (shopIds !== undefined && shopIds !== null) {
            shops = await ShopDao.findAllByIds(shopIds);
        }

        return await MenuDao.update(id, name, price, shops, products, resetShop, resetProduct);
    }

    static deleteById(id) {
        return MenuDao.delete(id);
    }

    /**
     *
     * @param id
     * @returns {Promise<Model|null>}
     */
    static getMenuProducts(id) {
        return MenuDao.getMenuProducts(id);

    }

}

module.exports = MenuController;
