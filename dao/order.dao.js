const Models = require('../models');
const Order = Models.Order;

class OrderDao {

    static async create(productsData, menusData, user, shop) {
        const order = await Order.create();

        for (let i = 0; i < productsData.length;i++) {

            await order.addProduct(productsData[i].product, {
                through:
                    {quantity : productsData[i].quantity}
            });
        }
        for (let i = 0; i < menusData.length;i++) {
            await order.addMenu(menusData[i].menu, {
                through:
                    { quantity : menusData[i].quantity}
            });
        }
        if (user !== null) {
            await order.setUser(user);
        }

        await order.setShop(shop);

        return order;
    }

    static findById(orderId) {
        return Order.findByPk(orderId, {
            paranoid: false,
            include: [{
                model: Models.Menu
            }, {
                model: Models.Product
            }, {
                model: Models.Shop,
                required: true
            }, {
                model: Models.User,
                required: false
            }]
        });
    }

    static findAll(paranoid) {
        return Order.findAll({
            paranoid: paranoid,
            include: [{
                model: Models.Menu
            }, {
                model: Models.Product
            }]
        });
    }

    static findAllByShop(shopId, paranoid) {
        return Order.findAll({
            where: {
                shop_id: shopId
            },
            paranoid: paranoid,
            include: [{
                model: Models.Menu,
                required: false
            }, {
                model: Models.Product,
                required: false
            }, {
                model: Models.Shop,
                required: true
            }]
        });
    }

    static findAllByUser(userId, paranoid) {
        return Order.findAll({
            where: {
                user_id: userId
            },
            paranoid: paranoid,
            include: [{
                model: Models.Menu,
                required: false,
                include : [{
                    model: Models.Promotion,
                    required: false
                }]
            }, {
                model: Models.Product,
                required: false,
                include : [{
                    model: Models.Promotion,
                    required: false
                }]
            }, {
                model: Models.User,
                required: true
            }]
        });
    }

    static async addProducts(order, productsData) {
        for (let i = 0; i < productsData.length;i++) {
            await order.addProduct(productsData[i].product, {
                through:
                    { quantity : productsData[i].quantity}
            });
        }
    }

    static async addMenus(order, menusData) {
        for (let i = 0; i < menusData.length;i++) {
            await order.addMenu(menusData[i].menu, {
                through:
                    { quantity : menusData[i].quantity}
            });
        }
    }

    static async update(id, productsData, menusData, user, shop, resetProduct, resetMenu) {
        let order = await this.findById(id);
        if (user !== undefined && user !== null) {
            order.setUser(user)
        }
        if (shop !== undefined && shop !== null) {
            order.setShop(shop)
        }
        order = await order.save();

        if (resetProduct) {
            await order.removeProducts(order.Products.map(product => product.id));
        }
        await OrderDao.addProducts(order, productsData);


        if (resetMenu) {
            await order.removeMenus(order.Menus.map(menu => menu.id));
        }
        await OrderDao.addMenus(order, menusData);

        return await this.findById(id);

    }
    static delete(id) {
        return Order.destroy({
            where : {
                id: id
            }
        });
    }
}

module.exports = OrderDao;
