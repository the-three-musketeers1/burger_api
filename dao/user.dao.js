const models = require('../models');
const User = models.User;
const Role = models.Role;
const Session = models.Session;

class UserDao {

    /**
     * @param login {string}
     * @param email {string}
     * @param password {string}
     * @returns {Promise<User>}
     */
    static createUser(login, email, password) {
        return User.create({
            login,
            email,
            password
        })
    }

    /**
     * @param email {string}
     * @returns {Promise<User | null>}
     */
    static findUserByEmail(email) {
        return User.findOne({
            where: {
                email
            }
        });
    }

    /**
     * @param email {string}
     * @param password {string}
     * @returns {Promise<User | null>}
     */
    static findUserByEmailAndPassword(email, password) {
        return User.findOne({
            where: {
                email,
                password
            }
        });
    }

    /**
     * @param id {number}
     * @param exclude {string[]}
     * @param raw {boolean}
     * @returns {Promise<User | null>}
     */
    static findUserById(id, exclude, raw) {
        return User.findOne({
            where: {
                id
            },
            attributes: {
                exclude
            },
            raw
        });
    }

    /**
     * @param token
     * @returns {Promise<User | null>}
     */
    static findUserBySessionToken(token) {
        return User.findOne({
            include: [{
                model: Session,
                where: {
                    token
                }
            }]
        });
    }

    /**
     * @param exclude {string[]}
     * @param raw {Boolean}
     * -if raw is true, get only array of object with properties of users,
     * -if raw is false, get every object of users and metadata of request
     */
    static findAllUsers(exclude, raw) {
        return User.findAll({
            raw: raw,
            attributes: {
                exclude
            }
        });
    }

    /**
     * @param id {number}
     * @param login {string}
     * @param email {string}
     * @param password {string}
     * @returns {Promise<void>}
     */
    static async updateUser(id, login, email, password) {
        await User.update({
            login,
            email,
            password
        }, {
            where: {
                id
            }
        });
    }

    /**
     * @param id {number}
     * @param force {boolean}
     * @returns {Promise<void>}
     */
    static async destroyUserById(id, force) {
        await User.destroy({
            where: {
                id
            },
            force
        });
    }

    /**
     * @param user {User}
     * @param role {Role}
     * @returns {Promise<void>}
     */
    static async addRole(user, role) {
        return user.addRole(role);
    }

    /**
     * @param user {User}
     * @param raw {boolean}
     * @returns {Promise<Role[]>}
     */
    static async getUserRoles(user, raw) {
        return user.getRoles({
            raw
        });
    }

    /**
     * @param user {User}
     * @param role {Role}
     * @returns {Promise<void>}
     */
    static async removeUserRole(user, role) {
        await user.removeRole(role);
    }

    static async replaceUserRole(user, oldRole, newRole) {
        await user.removeRole(oldRole);
        await user.addRole(newRole);
    };
}

module.exports = UserDao;
