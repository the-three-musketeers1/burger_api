const models = require('../models');
const Product = models.Product;
const IngredientDao = require('./ingredient.dao');
const Recipe = models.Recipe;

class ProductDao {


    static async deleteProductIngredient(productId, ingredientId) {
        const product = await this.findProductById(productId);
        const ingredient = await IngredientDao.findIngredientById(ingredientId);
        return await product.removeIngredient(ingredient);
    }

    static findAllByIds(ids) {
        return Product.findAll({
            where : {
                id: ids
            }
        });
    }

    static async addProductIngredient(productId, ingredientsData) {
        const product = await this.findProductById(productId);
        await this.addProductRecipes(ingredientsData, product);
        return product;
    }

    static async createProduct(name, price, ingredientsData, ingredients) {
        const product = await Product.create({
            name,
            price
        });

        for (let i = 0; i < ingredientsData.length;i++) {
            await product.addIngredient(ingredients[i], {
                through:
                    {quantity : ingredientsData[i].quantity,
                    unit: ingredientsData[i].unit}
            });
        }

        return product;
    }

    static async findProductById(id) {
        const product = await Product.findByPk(id, {
            include: [{
                model: models.Ingredient
            }, {
                model: models.Promotion
            }]
        });

        if(!product) {
            throw 'Product not found';
        }
        return product;
    }

    static async findAllProducts() {
        return Product.findAll({
            include: [{
                model: models.Ingredient,
                required: true
            }, {
                model: models.Promotion
            }]
        });
    }

    static async deleteProduct(id){
        const product = await this.findProductById(id);

        for (const ingredient of product.Ingredients) {
            product.removeIngredient(ingredient);
        }

        return product.destroy({
            cascade: true
        });
    }

    static async updateProduct(id, name, price, ingredientsData) {
        const product = await this.findProductById(id);

        await product.update({
           name,
           price
        });

        if(ingredientsData && ingredientsData.length > 0) {
            const productRecipes = await this.findProductRecipes(id);
            await this.updateRecipes(productRecipes, ingredientsData);
            await product.setIngredients([]);
            await this.addProductRecipes(productRecipes, product);
        }

        return product;
    }

    static async updateRecipes(recipes, ingredientsData) {
        for(const ingredientData of ingredientsData) {
            const index = await recipes.findIndex((recipe) => parseInt(recipe.IngredientId) === parseInt(ingredientData.id));

            if(index !== -1) {
                if(ingredientData.quantity) {
                    recipes[index].quantity = ingredientData.quantity;
                }

                if(ingredientData.unit) {
                    recipes[index].unit = ingredientData.unit;
                }
            }
        }
    }

    static async addProductRecipes(recipes, product) {
        for(const recipe of recipes) {
            const ingredient = await IngredientDao.findIngredientById(recipe.IngredientId);
            await product.addIngredient(ingredient, {
                through: {
                    quantity: recipe.quantity,
                    unit: recipe.unit
                }
            });
        }
    }


    static async findProductRecipes(productId) {
        const recipes = await Recipe.findAll({
            where: {
                ProductId: productId
            }
        });

        if(!recipes) {
            throw 'Recipes not found';
        }

        return recipes;
    }

}

module.exports = ProductDao;
