const Ingredient = require('../models').Ingredient;

class IngredientDao {

    /**
     *
     * @param name
     * @returns {Promise<Ingredient>}
     */
    static createIngredient(name) {
        return Ingredient.create({
            name
        });
    }

    static findAllIngredients() {
        return Ingredient.findAll();
    }

    static async findIngredientById(id) {
        const ingredient = await Ingredient.findByPk(id);

        if(!ingredient) {
            throw 'Ingredient not found';
        }

        return ingredient;
    }

    static async deleteIngredient(id) {
        return Ingredient.destroy({
            where: {
                id
            }
        });
    }

    static async updateIngredient(id, name) {
        return await Ingredient.update({
            name
        }, {
            where: {
                id
            }
        });
    }

}

module.exports = IngredientDao;