const Sequelize = require('sequelize');
const models = require('../models');
const Role = models.Role;
const User = models.User;

class RoleDao {

    /**
     * @param name {string}
     * @returns {Promise<Role>}
     */
    static async createRole(name) {
        return Role.create({
            name
        });
    }

    /**
     * @param name {string}
     * @returns {Promise<Role|null>}
     */
    static async findRoleByName(name) {
        return Role.findOne({
            where: {
                name
            }
        });
    }

    /**
     * @param raw {boolean}
     * @returns {Promise<any[]|<Role[]>>}
     */
    static async findAllRoles(raw) {
        return Role.findAll({
            raw
        });
    }

    /**
     * @param id {number}
     * @returns {Promise<Role|null>}
     */
    static async findRoleById(id) {
        return Role.findOne({
            where: {
                id
            }
        });
    }

    /**
     * @param id {number}
     * @param name {string}
     * @returns {Promise<void>}
     */
    static async updateRole(id, name) {
        await Role.update({
            name
        }, {
            where: {
                id
            }
        });
    }

    /**
     * @param roleId {number}
     * @param force {boolean}
     * @returns {Promise<void>}
     */
    static async destroyRoleById(roleId, force) {
        await Role.destroy({
            where: {
                id: roleId
            },
            force
        });
    }

    /**
     * @param name {string}
     * @param force {boolean}
     * @returns {Promise<void>}
     */
    static async destroyRoleByName(name, force) {
        await Role.destroy({
            where: {
                name
            },
            force
        });
    }

    /**
     * @param roleId {number}
     * @param userId {number}
     * @param raw {boolean}
     * @returns {Promise<Role>}
     */
    static async findRoleByRoleAndUserIds(roleId, userId, raw) {
        return Role.findOne({
            where: {
                id: roleId
            },
            include: [{
                model: User,
                where: {id: userId}
            }],
            raw
        });
    }
}

module.exports = RoleDao;