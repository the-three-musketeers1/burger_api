const Session = require('../models').Session;

class SessionDao {

    /**
     * @param token {string}
     * @returns {Promise<Session>}
     */
    static createSession(token) {
        return Session.create({
            token
        });
    }

    /**
     * @param sessionId {number}
     * @returns {Promise<Session | null>}
     */
    static findSessionById(sessionId) {
        return Session.findOne({
            where: {
                id : sessionId
            }
        });
    }

    /**
     * @param userId {number}
     * @returns {Promise<Session | null>}
     */
    static findSessionByUserId(userId) {
        return Session.findOne({
            where: {
                user_id: userId
            }
        });
    }

    /**
     * @param user {User}
     * @returns {Promise<Session[] | []>}
     */
    static findSessionsByUser(user) {
        return user.getSessions();
    }

    /**
     * @param session {Session}
     * @param user {User}
     * @returns {Promise<void>}
     */
    static async setUser(session, user) {
        await session.setUser(user);
    };

    /**
     * @param sessionId {number}
     * @param force {boolean}
     * @returns {Promise<void>}
     */
    static async destroySessionById(sessionId, force) {
        await Session.destroy({where: {id: sessionId}, force});
    }

    /**
     * @param sessionsId {number[]}
     * @param force {boolean}
     * @returns {Promise<void>}
     */
    static async destroySessionBySessionsId(sessionsId, force) {
        await Session.destroy({where: {id: sessionsId}, force});
    }
}

module.exports = SessionDao;
