module.exports = {
    ShopDao: require('./shop.dao'),
    IngredientDao: require('./ingredient.dao'),
    MenuDao: require('./menu.dao'),
    UserDao: require('./user.dao'),
    SessionDao: require('./session.dao'),
    PromotionDao: require('./promotion.dao'),
    ProductDao: require('./product.dao'),
    RoleDao: require('./role.dao'),
    OrderDao: require('./order.dao')
};
