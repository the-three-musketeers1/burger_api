const Shop = require('../models').Shop;
const Stock = require('../models').Stock;
const IngredientDao = require('./ingredient.dao');
const ProductDao = require('./product.dao');

class ShopDao {

    /**
     *
     * @param name
     * @param address
     * @returns {Promise<Shop>}
     */
    static createShop(name, address) {
        return Shop.create({
            name,
            address
        });
    }

    static async addStockToShop(id, ingredientsData, ingredients) {
        const shop = await this.findShopById(id);

        for (let i = 0; i < ingredientsData.length;i++) {
            await shop.addIngredient(ingredients[i], {
                through:
                    {quantity : ingredientsData[i].quantity,
                        unit: ingredientsData[i].unit}
            });
        }

        return shop;
    }

    static async findStocksOfShop(id) {
        const shop = await this.findShopById(id);
        return await shop.getIngredients();
    }

    static findAllShops() {
        return Shop.findAll();
    }

    /**
     *
     * @param ids : number[]
     * @returns {Promise<Shop[]>}
     */
    static findAllByIds(ids) {
        return Shop.findAll({
            where : {
                id : ids
            }
        });
    }

    static async findShopById(id) {
        const shop = await Shop.findByPk(id);

        if(!shop) {
            throw 'Shop not found';
        }

        return shop;
    }

    static async deleteShop(id) {
        return Shop.destroy({
            where: {
                id
            }
        });
    }

    static async deleteStock(id, ingredientId) {
        const shop = await this.findShopById(id);
        const ingredient = await IngredientDao.findIngredientById(ingredientId);
        return await shop.removeIngredient(ingredient);
    }

    static async updateShop(id, name, address, ingredientsData) {
        const shop = await this.findShopById(id);

        await shop.update({
            name,
            address
        });

        if(ingredientsData && ingredientsData.length > 0) {
            const shopStock = await this.findAllStocks(id);
            await ProductDao.updateRecipes(shopStock, ingredientsData);
            await shop.setIngredients([]);
            await ProductDao.addProductRecipes(shopStock, shop);
        }

        return shop;
    }



    static async findAllStocks(id) {
        const stocks = await Stock.findAll({
            where: {ShopId: id}
        });

        if(!stocks) {
            throw 'Stock not found';
        }

        return stocks;
    }

}

module.exports = ShopDao;
