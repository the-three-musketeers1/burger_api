const Models = require('../models');
const Menu = Models.Menu;
class MenuDao {


    static async create(name, price, products, shops) {
        const menu = await Menu.create({
            name,
            price,
        });
        if (products.length > 0) {
            await menu.addProducts(products);
        }

        if (shops.length > 0) {
            await menu.addShops(shops);
        }

        return menu;
    }

    static findById(id) {
        return Menu.findByPk(id, {
            include: [{
                model: Models.Product,
                required: false
            }, {
                model: Models.Promotion,
                required: false
            }]
        });
    }

    static findAll() {
        return Menu.findAll();
    }

    static findAllByIds(ids) {
        return Menu.findAll({
            where: {
                id: ids
            }
        });
    }

    static findAllByShop(shop) {
        return shop.getMenus();
    }

    static delete(id) {
        return Menu.destroy({
            where: {
                id
            }
        });
    }

    static async update(id, name, price, shops, products, resetShops, resetProducts) {

        let menu = await this.findById(id);
        if (name !== undefined && name !== null) {
            menu.name = name;
        }
        if (price !== undefined && price !== null) {
            menu.price = price;
        }
        menu = await menu.save();
        if (shops != null) {
            if (resetShops) {
                await MenuDao.setShops(menu, shops);
            } else {
                await MenuDao.addShops(menu, shops)
            }
        }
        if (products != null) {
            if (resetProducts) {
                await MenuDao.setProducts(menu, products);
            } else {
                await MenuDao.addProducts(menu, products)
            }
        }

        return menu
    }

    static async addShops(menu, shops) {
        await menu.addShops(shops);
    }

    static async setShops(menu, shops) {
        await menu.setShops(shops);
    }

    static async setProducts(menu, products) {
        await menu.setProducts(products);
    }

    static async addProducts(menu, products) {
        await menu.addProducts(products);
    }

    /**
     *
     * @param id
     * @returns {Promise<Model | null>}
     */
    static getMenuProducts(id) {
        let menuIngredient = Menu.findByPk(id, {
            include: [{
                model: Models.Product,
                required: true
            }]
        });
        return menuIngredient;
    }
}

module.exports = MenuDao;
