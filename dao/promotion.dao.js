const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const models = require('../models');
const MenuDao = require('./menu.dao');
const ProductDao = require('./product.dao');
const Promotion = models.Promotion;
const Menu = models.Menu;
const Product = models.Product;


class PromotionDao {

    static async createMenuPromotion(menuId, price, in_front, start_date, end_date) {

        const menu = await MenuDao.findById(menuId);

        if(menu) {
            const promotion = await Promotion.create({
                price,
                in_front,
                start_date,
                end_date
            });

            await promotion.setMenu(menu);
            return promotion;
        }

        return null;
    }

    static async createProductPromotion(productId, price, in_front, start_date, end_date) {

        const product = await ProductDao.findProductById(productId);

        if(product) {
            const promotion = await Promotion.create({
                price,
                in_front,
                start_date,
                end_date
            });

            await promotion.setProduct(product);
            return promotion;
        }

        return null;
    }

    static async findMenuPromotions(menuId, deadline) {
        return Promotion.findAll({
            where: {
                "menu_id": menuId,
                end_date: {
                    [Op.gte]: new Date(deadline)
                }
            }
        });
    }

    static async findProductPromotions(productId, deadline) {
        return Promotion.findAll({
            where: {
                "product_id": productId,
                end_date: {
                    [Op.gte]: new Date(deadline)
                }
            }
        });
    }

    static async findAllPromotions(deadline) {
        return Promotion.findAll({
            where: {
                end_date: {
                    [Op.gte]: new Date(deadline)
                }
            },
            include: [{
                model: Menu
            }, {
                model: Product
            }]
        });
    }

    static async findPromotionById(id) {
        const promotion = await Promotion.findByPk(id, {
            include: [{
                model: Product
            }, {
                model: Menu
            }]
        });

        if(!promotion) {
            throw "Promotion not found";
        }

        return promotion;
    }

    static async deletePromotion(id) {
        return Promotion.destroy({
            where: {
                id
            }
        });
    }

    static async isPromotionMenu(id) {
        const promotion = await this.findPromotionById(id);
        if(await promotion.getMenu()) {
            return true;
        }
        return false;
    }

    static async isPromotionProduct(id) {
        const promotion = await this.findPromotionById(id);
        if(await promotion.getProduct()) {
            return true;
        }
        return false;
    }

    static async updatePromotionProduct(id, product_id) {
        const promotion = await this.findPromotionById(id);
        const product = await ProductDao.findProductById(product_id);

        if(promotion && product) {
            return await promotion.setProduct(product);
        }
        return null;
    }

    static async updatePromotionMenu(id, menu_id) {
        const promotion = await this.findPromotionById(id);
        const menu = await MenuDao.findById(menu_id);

        if(promotion && menu) {
            return await promotion.setMenu(menu);
        }
        return null;
    }

    static async updatePromotionAttributes(id, price, in_front,start_date, end_date) {
        return await Promotion.update({
            price,
            in_front,
            start_date,
            end_date
        }, {
            where: {
                id
            }
        });
    }

}

module.exports = PromotionDao;