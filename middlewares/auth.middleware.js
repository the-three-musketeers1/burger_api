const SecurityUtil = require('../utils').SecurityUtil;
const dao = require('../dao');
const UserDao = dao.UserDao;
const SessionDao = dao.SessionDao;
const AuthController = require('../controllers').AuthController;
const authController = new AuthController(UserDao, SessionDao, SecurityUtil);

class AuthMiddleware {

    /**
     * Check if header authorization value correspond to user session's token
     * affect user in request
     * @returns {function(...[*]=)}
     */
    static auth() {
        return async function (req, res, next) {
            const authorization = req.headers['authorization'];

            if (!authorization || !authorization.startsWith('Bearer ')) {
                res.status(401).end();
                return;
            }

            const token = authorization.slice(7);
            const user = await authController.userFromToken(token);

            if (!user) {
                res.status(403).end();
                return;
            }
            req.user = user;
            next();
        }
    }

    static authIfSessionExist() {
        return async function(req, res, next) {
            const authorization = req.headers['authorization'];

            if (!authorization || !authorization.startsWith('Bearer ')) {
                req.user = null;
                next();
                return;
            }
            const token = authorization.slice(7);
            const user = await authController.userFromToken(token);

            if (!user) {
                res.status(403).end();
                return;
            }
            req.user = user;
            next();
        }
    }
}

module.exports = AuthMiddleware;
