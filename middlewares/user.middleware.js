const UserController = require('../controllers').UserController;
const AuthMiddleware = require('./auth.middleware');

class UserMiddleware {
    /**
     * Check if user and if params id is subscribe user id
     * @returns {function(req, res, next)}
     */
    static checkUserSameThanParamsId() {
        return async function (req, res, next) {
            if (req.user === undefined || req.params === undefined || req.user.id !== parseInt(req.params.id)) {
                res.status(403).end();
                return;
            }
            next();
        }
    }

    /**
     * Check if user have one role in array of roles
     * @param roles {string[]}
     * @returns {function(req, res, next)}
     */
    static checkIfAuthHaveRole(roles) {
        return async function (req, res, next) {
            const user = req.user;
            if (user === undefined) {
                res.status(403).end();
                return;
            }
            const userRoles = await UserController.getUserRoles(user);
            const result = userRoles.some(userRole => {
                if (roles.some(role => userRole.name === role)) {
                    UserMiddleware.addRoleInRequest(req, userRole.name);
                    return true;
                }
                return false;
            });
            if (!result) {
                res.status(403).end();
                return;
            }

            next();
        }
    }

    static addRoleInRequest(req, role) {
        if (req.roles === undefined) {
            req.roles = [role];
            return;
        }
        req.roles.push(role);
    }

    /**
     * array of middleware to check authentication and if param id correspond to auth user
     * @returns {(function(req, res, next))[]}
     * @constructor
     */
    static authSameThanParamsId() {
        return [
            AuthMiddleware.auth(),
            this.checkUserSameThanParamsId()
        ];
    }

    /**
     * array of middleware to check if auth is admin
     * @returns {(function(req, res, next))[]}
     */
    static checkIfAuthIsAdmin() {
        return [
            AuthMiddleware.auth(),
            this.checkIfAuthHaveRole(['admin'])
        ];
    }

    /**
     * array of middleware to check if auth is at least admin
     * @returns {(function(req, res, next))[]}
     */
    static checkIfAuthIsAtLeastPreparer() {
        return [
            AuthMiddleware.auth(),
            this.checkIfAuthHaveRole(['admin', 'preparer'])
        ];
    }

    static checkIfAuthIsAtLeastCustomer() {
        return [
            AuthMiddleware.auth(),
            this.checkIfAuthHaveRole(['admin', 'preparer', 'customer'])
        ];
    }
}

module.exports = UserMiddleware;
