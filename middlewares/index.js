module.exports = {
    AuthMiddleware: require('./auth.middleware'),
    validate: require('./validate.middleware'),
    UserMiddleware: require('./user.middleware')
};
