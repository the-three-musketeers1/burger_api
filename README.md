Burger_Api

Cette application est une API permettant de réaliser des commandes aux bornes d’un fast food,
d'un téléphone et de gérer aussi les commandes en cuisine.

3 modes sont disponibles:
* Admin:  permet de gérer les produits, menu, promotions, mises en avant, ainsi que la possibilité de
récupérer et de traiter les commandes. L’utilisateur admin doit obligatoirement être connecté à l’aide d’un token de session.
* Customer: permet de voir les produits, menu, promotions, ainsi que de passer des commandes en
magasin et il n'est pas obligé de se connecter.
* Préparateur: permet de voir les les commandes et de les traiter. L’utilisateur préparateur doit
obligatoirement être connecté à l’aide d’un token de session.

Pour finir l'installation du projet :
* Dans le dossier config, copier le fichier 'config.json.sample' et coller le dans le même dossier en le nommant 'config.json'
* Remplir les informations dans le fichier 'config.json'
* copier le fichier '.env.sample' et coller le dans le même dossier en le nommant '.env'
* Remplir les informations dans le fichier '.env'
* Remplir correctement config.json pour la connection à la base de données
* Exécuter la commande: npm install
* Exécuter la commande: npm run seed
	
Pour démarrer l'application :
* npm run start

Les requêtes sequelize sont de base non visible.
Pour voir le log :
* Allez dans le fichier /config/config.json
* Dans la configuration de développement ou de test, mettre l'option logging à true

Pour dev avec Nodemon ❤️ : 
* npm install pour que ça installe nodemon présent dans le package.json
* npm run watch OU créer une nouvelle configuration pour lancer le script "watch" présent dans le fichier package.json

Pour l'authentification, le schéma utilisé est 'Bearer authentication'.

La documentation est effectuée par le bel outil Swagger.
Pour lire la documentation :
* Exécuter la commande : npm run start
* Aller sur http://{host sur swagger.json}/api-docs
