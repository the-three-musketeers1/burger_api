require('dotenv').config();
const express = require('express');
const models = require('./models');
const routes = require('./routes');
const PORT = process.env.PORT || 3001;

async function bootstrap() {

    await models.sequelize.authenticate();
    await models.sequelize.sync();
    const app = express();
    var swaggerUi = require('swagger-ui-express'),
        swaggerDocument = require('./swagger.json');

    routes(app);
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    return app.listen(PORT, () => console.log('Server started on ' + PORT))
}

module.exports = bootstrap();
