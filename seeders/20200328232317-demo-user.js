'use strict';
const SecurityUtils = require('../utils').SecurityUtil;

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('User', [
            {
                login: 'Skyzz',
                email: 'skyzz@burgerapi.com',
                password: SecurityUtils.hashPassword('1234'),
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                login: 'JeremTer',
                email: 'jeremter@burgerapi.com',
                password: SecurityUtils.hashPassword('5678'),
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                login: 'Tkt',
                email: 'tkt@burgerapi.com',
                password: SecurityUtils.hashPassword('91011'),
                created_at: new Date(),
                updated_at: new Date()
            }
        ]);
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('User', null, {});
    }
};
