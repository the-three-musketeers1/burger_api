'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('Role', [
            {
                name: 'customer',
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: 'preparer',
                created_at: new Date(),
                updated_at: new Date()
            },
            {
                name: 'admin',
                created_at: new Date(),
                updated_at: new Date()
            }
        ], {})
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('Role', null, {});
    }
};
