'use strict';
const dao = require('../dao');
const UserDao = dao.UserDao;
const RoleDao = dao.RoleDao;

module.exports = {
    up: async (queryInterface, Sequelize) => {
        const adminsEmail = [
            'skyzz@burgerapi.com',
            'jeremter@burgerapi.com',
            'tkt@burgerapi.com'
        ];
        const admins = [];
        const adminRole = await RoleDao.findRoleByName('admin');

        for (let i = 0; i < adminsEmail.length; i++) {
            const user = await UserDao.findUserByEmail(adminsEmail[i]);
            admins.push(user);
        }

        return await queryInterface.bulkInsert('User_Roles', [
            {
                created_at: new Date(),
                updated_at: new Date(),
                role_id: await adminRole.id,
                user_id: await admins[0].id
            },
            {
                created_at: new Date(),
                updated_at: new Date(),
                role_id: await adminRole.id,
                user_id: await admins[1].id
            },
            {
                created_at: new Date(),
                updated_at: new Date(),
                role_id: await adminRole.id,
                user_id: await admins[2].id
            }
        ]);
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('User_Roles', null, {});
    }
};
