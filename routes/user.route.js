const bodyParser = require('body-parser');
const middleware = require('../middlewares');
const UserMiddleware = middleware.UserMiddleware;
const UserController = require('../controllers').UserController;
const validators = require('../validators');
const SharedValidator = validators.SharedValidator;
const UserValidator = validators.UserValidator;
const validate = require('../middlewares').validate;

module.exports = function (app) {

    app.use('/users', UserMiddleware.checkIfAuthIsAdmin());

    app.get('/users', async (req, res, next) => {
        try {
            const users = await UserController.getUsers();
            res.status(200).json(users);
        } catch (e) {
            res.status(500).end();
        }
    });

    app.get('/users/:id', SharedValidator.isParamInt('id'), validate, async (req, res, next) => {
        try {
            const user = await UserController.getUser(req.params.id);
            if (!user) {
                res.status(404).end();
                return;
            }
            res.status(200).json(user);
        } catch (e) {
            res.status(500).end();
        }
    });

    app.put('/users/:id', bodyParser.json(), UserValidator.putValidatorRules(), validate, async (req, res, next) => {
        try {
            const user = await UserController.updateUser(req.params.id, req.body);
            if (!user) {
                res.status(404).end();
                return;
            }
            res.status(200).json(user);
        } catch (err) {
            res.status(500).end();
        }
    });

    app.delete('/users/:id', SharedValidator.isParamInt('id'), validate, async (req, res, next) => {
        try {
            const result = await UserController.deleteUser(req.params.id);
            if (!result) {
                res.status(404).end();
                return;
            }
            res.status(204).end();
        } catch (e) {
            res.status(500).end();
        }
    });

    app.post('/users/:userId/roles/:roleId', UserValidator.areParamsUserRoleIdsInt(), validate, async (req, res, next) => {
        const userId = req.params.userId;
        const roleId = req.params.roleId;
        try {
            const result = await UserController.addRole(userId, roleId);
            res.status(201).json(result);
        } catch (e) {
            if (e.message.length > 0) {
                res.status(404).send(e.message);
                return;
            }
            res.status(500).end();
        }
    });

    app.get('/users/:userId/roles', SharedValidator.isParamInt('userId'), validate, async (req, res, next) => {
        let userId = req.params.userId;
        try {
            const roles = await UserController.getUserRolesByUserId(userId);
            if (roles === null) {
                res.status(404).end();
                return;
            }
            res.status(200).json(roles);
        } catch (e) {
            res.status(500).end();
        }
    });

    app.get('/users/:userId/roles/:roleId', UserValidator.areParamsUserRoleIdsInt(), validate, async (req, res, next) => {
        const userId = req.params.userId;
        const roleId = req.params.roleId;

        try {
            const role = await UserController.getUserRoleByIds(userId, roleId);
            res.status(200).json(role);
        } catch (e) {
            if (e.message.length > 0) {
                res.status(404).send(e.message);
                return;
            }
            res.status(500).end();
        }
    });

    app.put('/users/:userId/roles/:roleId',
        bodyParser.json(), UserValidator.putUserRoleValidationRules(), validate,
        async (req, res, next) => {
            const userId = req.params.userId;
            const oldRoleId = req.params.roleId;
            const newRoleId = req.body.roleIdToPut;

            try {
                const result = await UserController.putUserRole(userId, oldRoleId, newRoleId);
                if (!result) {
                    res.status(404).end();
                    return;
                }
                res.status(204).end();
            } catch (e) {
                res.status(500).end();
            }
        });

    app.delete('/users/:userId/roles/:roleId', UserValidator.areParamsUserRoleIdsInt(), validate, async (req, res, next) => {
        let userId = req.params.userId;
        let roleId = req.params.roleId;

        try {
            const result = await UserController.deleteUserRole(userId, roleId);
            if (!result) {
                res.status(404).end();
                return;
            }
            res.status(204).end();
        } catch (e) {
            res.status(500).end();
        }
    });
};
