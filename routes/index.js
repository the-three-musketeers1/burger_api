const authRoutes = require('./auth.route');
const ingredientRoutes = require('./ingredient.route');
const menuRoutes = require('./menu.route');
const orderRoutes = require('./order.route');
const productRoutes = require('./product.route');
const promotionRoutes = require('./promotion.route');
const roleRoutes = require('./role.route');
const shopRoutes = require('./shop.route');
const userRoutes = require('./user.route');

module.exports = function (app) {
    authRoutes(app);
    ingredientRoutes(app);
    menuRoutes(app);
    orderRoutes(app);
    productRoutes(app);
    promotionRoutes(app);
    roleRoutes(app);
    shopRoutes(app);
    userRoutes(app);
};
