const PromotionValidator = require('../validators/promotion.validator');
const middleware = require('../middlewares');
const bodyParser = require('body-parser');
const PromotionController = require('../controllers').PromotionController;
const validate = middleware.validate;
const UserMiddleware = middleware.UserMiddleware;

module.exports = function(app) {

    app.post('/promotion/menu/:menuId', UserMiddleware.checkIfAuthIsAdmin(), bodyParser.json(),
        PromotionValidator.createMenuPromotionValidationRules(), validate, async (req, res) => {
        try {
            const menuId = parseInt(req.params.menuId);
            const promotion = await PromotionController.createMenuPromotion(menuId,
                                                                            req.body.price,
                                                                            req.body.in_front,
                                                                            req.body.start_date,
                                                                            req.body.end_date);
            res.status(201).json(promotion);
        } catch (e) {
            res.status(406).end();
        }
    });

    app.post('/promotion/product/:productId', UserMiddleware.checkIfAuthIsAdmin(), bodyParser.json(),
        PromotionValidator.createProductPromotionValidationRules(), validate, async (req, res) => {
        try {
            const productId = parseInt(req.params.productId);
            const promotion = await PromotionController.createProductPromotion(productId,
                                                                            req.body.price,
                                                                            req.body.in_front,
                                                                            req.body.start_date,
                                                                            req.body.end_date);
            res.status(201).json(promotion);
        } catch (e) {
            res.status(406).end();
        }
    });

    app.get('/promotion/menu/:menuId/:deadline', bodyParser.json(),
        PromotionValidator.getMenuPromotionValidationRules(), validate, async (req, res) => {
        try {
            const menuId = parseInt(req.params.menuId);
            const promotions = await PromotionController.findMenuPromotions(menuId, req.params.deadline);
            res.status(202).json(promotions);
        } catch (e) {
            res.status(404).end();
        }

    });

    app.get('/promotion/product/:productId/:deadline', bodyParser.json(),
        PromotionValidator.getProductPromotionValidationRules(), validate, async (req, res) => {

        try {
            const productId = parseInt(req.params.productId);
            const promotions = await PromotionController.findProductPromotions(productId, req.params.deadline);
            res.status(202).json(promotions);
        } catch (e) {
            res.status(404).end();
        }

    });


    app.get('/allPromotions/:deadline', bodyParser.json(), PromotionValidator.getAllPromotionsValidationRules(),
        validate, async (req, res) => {
        try {
            const promotion = await PromotionController.findAllPromotions(req.params.deadline);
            res.status(202).json(promotion);
        } catch (e) {
            res.status(404).end();
        }
    });


    app.get('/promotion/:id', PromotionValidator.idPromotionRule(), validate, async (req, res) => {
        try {
            const id = parseInt(req.params.id);
            const promotion = await PromotionController.findPromotionById(id);
            res.status(202).json(promotion);
        } catch (e) {
            res.status(404).end();
        }
    });


    app.delete('/promotion/:id', UserMiddleware.checkIfAuthIsAdmin(),
        PromotionValidator.idPromotionRule(), validate, async(req, res) => {
        try {
            const id = parseInt(req.params.id);
            await PromotionController.deletePromotion(id);
            res.status(204).end();
        } catch (e) {
            res.status(409).end();
        }
    });

    app.put('/promotion/:id', UserMiddleware.checkIfAuthIsAdmin(),
        bodyParser.json(),
        PromotionValidator.updatePromotionValidationRules(), validate, async (req, res) => {
        try {
            const id = parseInt(req.params.id);
            const promotion = await PromotionController.updatePromotion(id,
                req.body.price,
                req.body.in_front,
                req.body.start_date,
                req.body.end_date,
                req.body.product_id,
                req.body.menu_id);

            res.status(201).json(promotion);
        } catch (e) {
            console.error(e);
            res.status(406).end();
        }
    });


};