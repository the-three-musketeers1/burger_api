const bodyParser = require('body-parser');
const middleware = require('../middlewares');
const OrderController = require('../controllers').OrderController;
const AuthMiddleware = middleware.AuthMiddleware;
const UserMiddleware = middleware.UserMiddleware;

const OrderValidator = require('../validators').OrderValidator;
const validate = require('../middlewares').validate;

module.exports = function(app) {

    app.post('/order',AuthMiddleware.authIfSessionExist(),bodyParser.json(),
        OrderValidator.createOrderRules(), validate, async (req, res) => {
            try {
                const order = await OrderController.create(req.body.productData,
                    req.body.menuData,
                    req.user,
                    req.body.shopId
                );
                res.status(201).json(order);
            } catch (e) {
                console.error(e);
                res.status(406).end();
            }

    });

    app.put('/order/:id',UserMiddleware.checkIfAuthIsAtLeastCustomer() ,bodyParser.json(),
        OrderValidator.updateOrderRules(), validate, async (req, res) => {
            try {
                const order = await OrderController.update(req.params.id,
                    req.body.productData,
                    req.body.menuData,
                    req.body.userId,
                    req.body.shopId,
                    req.body.resetProduct,
                    req.body.resetMenu
                );
                res.status(201).json(order);
            } catch (e) {
                console.error(e);
                res.status(406).end();
            }

    });



    app.get('/order/:id',UserMiddleware.checkIfAuthIsAtLeastCustomer(),
        OrderValidator.checkId(), validate, async(req, res) => {
        try {
            const order = await OrderController.getById(req.params.id);
            res.status(201).json(order);
        } catch (e) {
            console.error(e);
            res.status(409).end();
        }
    });

    app.delete('/order/:id',UserMiddleware.checkIfAuthIsAtLeastCustomer(),
        OrderValidator.checkId(), validate, async (req, res) => {
            try {
                await OrderController.deleteById(req.params.id);
                res.status(204).end();
            } catch (e) {
                console.error(e);
                res.status(409).end();
            }
        });

    app.get('/currentOrders',UserMiddleware.checkIfAuthIsAtLeastPreparer(), async(req, res) => {
        try {
            const orders = await OrderController.getAll(true);
            res.status(201).json(orders);
        } catch (e) {
            console.error(e);
            res.status(409).end()
        }
    });

    app.get('/currentOrders/shop/:id',UserMiddleware.checkIfAuthIsAtLeastPreparer(),
        OrderValidator.checkId(), validate, async(req, res) => {
        try {
            const orders = await OrderController.getAllByShop(req.params.id, true);
            res.status(201).json(orders);
        } catch (e) {
            console.error(e);
            res.status(409).end()
        }
    });

    app.get('/currentOrders/user/:id',UserMiddleware.checkIfAuthIsAtLeastCustomer() ,
        OrderValidator.checkId(), validate, async(req, res) => {
        try {
            const orders = await OrderController.getAllByShop(req.params.id, true);
            res.status(201).json(orders);
        } catch (e) {
            console.error(e);
            res.status(409).end()
        }
    });


    app.get('/orders',UserMiddleware.checkIfAuthIsAtLeastPreparer(), async (req, res) => {
        try {
            const orders = await OrderController.getAll(false);
            res.status(201).json(orders);
        } catch (e) {
            console.error(e);
            res.status(409).end()
        }
    });

    app.get('/orders/shop/:id',UserMiddleware.checkIfAuthIsAtLeastPreparer(),
        OrderValidator.checkId(), validate, async (req, res) => {
        try {
            const orders = await OrderController.getAllByShop(req.params.id, false);
            res.status(201).json(orders);
        } catch (e) {
            console.error(e);
            res.status(409).end()
        }

    });

    app.get('/orders/user/:id',UserMiddleware.checkIfAuthIsAtLeastCustomer() ,
        OrderValidator.checkId(), validate, async(req, res) => {
        try {
            const orders = await OrderController.getAllByShop(req.params.id, false);
            res.status(201).json(orders);
        } catch (e) {
            console.error(e);
            res.status(409).end()
        }
    });

};
