const bodyParser = require('body-parser');
const MenuController = require('../controllers').MenuController;
const middleware = require('../middlewares');
const UserMiddleware = middleware.UserMiddleware;
const MenuValidator = require('../validators').MenuValidator;
const validate = require('../middlewares').validate;

module.exports = function(app) {
    app.post('/menu',UserMiddleware.checkIfAuthIsAdmin(), bodyParser.json(),
        MenuValidator.createMenuRules(),validate, async (req, res) => {
        const data = req.body;
        try {
            const menu = await MenuController.createMenu(data.name,
                                                        data.price,
                                                        data.productIds,
                                                        data.shopIds);
            res.status(201).json(menu);
        } catch (e) {
            console.error(e);
            res.status(406).end();
        }
    });

    app.get('/menu/:id',
        MenuValidator.checkId(), validate, async (req,res) => {
        try {
            const id = parseInt(req.params.id);
            const menu = await MenuController.getById(id);
            res.status(201).json(menu);
        } catch (e) {
            res.status(404).end();
        }
    });

    app.get('/menu/:id/products',
        MenuValidator.checkId(), validate,async (req, res) => {
        try {
            const id = parseInt(req.params.id);
            const menu = await MenuController.getMenuProducts(id);
            res.status(201).json(menu);
        } catch (e) {
            res.status(404).end();
        }
    });

    app.get('/menus', async (req, res) => {
        try {
            const menus = await MenuController.getAll();
            res.status(201).json(menus);
        } catch (e) {
            res.status(409).end();
        }
    });

    app.get('/menu/shop/:id',
        MenuValidator.checkId(), validate, async (req, res) => {
        try {
            const id = parseInt(req.params.id);
            const menus = await MenuController.getAllByShop(id);
            res.status(201).json(menus);
        } catch (e) {
            res.status(404).end();
        }
    });

    app.put('/menu/:id',UserMiddleware.checkIfAuthIsAdmin(), bodyParser.json(),
        MenuValidator.updateMenuRules(), validate,async (req, res) => {
        const data = req.body;
        try {
            const id = parseInt(req.params.id);
            const menu = await MenuController.update(
                id,
                data.name,
                data.price,
                data.productIds,
                data.shopIds,
                data.resetShop,
                data.resetProduct);
            res.status(201).json(menu);
        } catch (e) {
            res.status(406).end();
        }
    });

    app.delete('/menu/:id', UserMiddleware.checkIfAuthIsAdmin(),
        MenuValidator.checkId(), validate, async(req, res) => {
            try {
                const id = parseInt(req.params.id);
                await MenuController.deleteById(id);
                res.status(204).end();
            } catch (e) {
                res.status(409).end();
            }
    });
};
