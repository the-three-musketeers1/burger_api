const bodyParser = require('body-parser');
const RoleController = require('../controllers').RoleController;
const RoleValidator = require('../validators').RoleValidator;
const middleware = require('../middlewares');
const UserMiddleware = middleware.UserMiddleware;
const validate = middleware.validate;

module.exports = function (app) {

    app.use('/roles', UserMiddleware.checkIfAuthIsAdmin());

    app.post('/roles', bodyParser.json(), RoleValidator.checkNameValidationRules(), validate, async (req, res, next) => {
        try {
            const role = await RoleController.createRole(req.body.name);
            if (!role) {
                res.status(403).end();
                return;
            }

            res.status(201).json(role);
        } catch (e) {
            res.status(500).end();
        }
    });

    app.get('/roles', async (req, res, next) => {
        try {
            const roles = await RoleController.findAllRoles();
            res.status(200).json(roles);
        } catch (e) {
            res.status(500).end();
        }
    });

    app.get('/roles/:id', RoleValidator.checkIdValidationRules(), validate, async (req, res, next) => {
        try {
            const role = await RoleController.findRoleById(req.params.id);
            if (!role) {
                res.status(404).end();
                return;
            }
            res.status(200).json(role);
        } catch (e) {
            res.status(500).end();
        }
    });

    app.put('/roles/:id', bodyParser.json(), RoleValidator.checkIdNameValidationRules(), validate, async (req, res, next) => {
        try {
            const result = await RoleController.updateRole(req.params.id, req.body.name);
            if (!result) {
                res.status(404).end();
                return;
            }
            res.status(204).end();
        } catch (e) {
            res.status(500).end();
        }
    });

    app.delete('/roles/:id', RoleValidator.checkIdValidationRules(), validate, async (req, res, next) => {
        try {
            const result = await RoleController.deleteRole(req.params.id);
            if (!result) {
                res.status(404).end();
                return;
            }
            res.status(204).end();
        } catch (e) {
            res.status(500).end();
        }
    });
};
