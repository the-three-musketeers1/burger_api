const bodyParser = require('body-parser');
const AuthValidator = require('../validators').AuthValidator;
const validate = require('../middlewares').validate;

const dao = require('../dao');
const UserDao = dao.UserDao;
const SessionDao = dao.SessionDao;
const RoleDao = dao.RoleDao;
const SecurityUtil = require('../utils').SecurityUtil;
const AuthController = require('../controllers').AuthController;
const authController = new AuthController(UserDao, SessionDao, RoleDao, SecurityUtil);

const UserMiddleware = require('../middlewares').UserMiddleware;

module.exports = function (app) {
    app.post('/auth/subscribe', bodyParser.json(), AuthValidator.subscribeValidationRules(), validate,
        async (req, res) => {
            try {
                const user = await authController.subscribe(req.body.login,
                    req.body.email,
                    req.body.password);
                if (!user) {
                    res.status(403).end();
                    return;
                }
                res.status(201).json(user);
            } catch (err) {
                res.status(500).end();
            }
        });

    app.post('/auth/login', bodyParser.json(), AuthValidator.loginValidationRules(), validate,
        async (req, res) => {
            try {
                const session = await authController.login(req.body.email, req.body.password);
                if (session) {
                    res.status(201).json(session);
                } else {
                    res.status(401).end();
                }
            } catch (err) {
                if (err.message === 'user have already one session') {
                    res.status(403).end();
                    return;
                }
                res.status(500).end();
            }
        });

    app.delete('/auth/logout/:id', AuthValidator.logoutValidationRules(), validate,
        async (req, res) => {
            try {
                const session = await authController.logout(req.params.id);
                if (!session) {
                    res.status(404).end();
                    return;
                }
                res.status(204).end();
            } catch (err) {
                if (err.message === 'user undefined') {
                    res.status(404).end();
                    return;
                }
                res.status(500).end();
            }
        });

    app.put('/auth/subscribe/:id',
        bodyParser.json(),
        AuthValidator.putSubscribeValidationRules(),
        validate,
        UserMiddleware.authSameThanParamsId(),
        async (req, res, next) => {
            let subscribeId = req.params.id;
            let login = req.body.login;
            let email = req.body.email;
            let password = req.body.password;
            try {
                await authController.updateSubscriber(subscribeId, login, email, password);
                res.status(204).end();
            } catch (e) {
                res.status(500).end();
            }
        });

    app.delete('/auth/subscribe/:id',
        AuthValidator.deleteValidationRules(),
        validate,
        UserMiddleware.authSameThanParamsId(),
        async (req, res, next) => {
        try {
            const result = await authController.deleteSubscribe(req.params.id);
            if (!result) {
                res.status(404).end();
                return;
            }
            res.status(204).end();
        } catch (e) {
            res.status(500).end();
        }
    });
};
