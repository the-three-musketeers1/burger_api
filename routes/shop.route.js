const bodyParser = require('body-parser');
const ShopController = require('../controllers').ShopController;
const middleware = require('../middlewares');
const UserMiddleware = middleware.UserMiddleware;
const ShopValidator = require('../validators').ShopValidator;
const validate = require('../middlewares').validate;

module.exports = function(app) {

    app.post('/shop', UserMiddleware.checkIfAuthIsAdmin(), bodyParser.json(),
        ShopValidator.createShopRules(), validate, async (req, res) => {
        try {
            const shop = await ShopController.createShop(req.body.name, req.body.address);
            res.status(201).json(shop);
        } catch (e) {
            res.status(406).end();
        }
    });

    app.post('/shop/:id/stock', UserMiddleware.checkIfAuthIsAdmin(), bodyParser.json(),
        ShopValidator.addStockValidationRules(), validate, async (req, res) => {
            try {
                const shop = await ShopController.addStockToShop(req.params.id, req.body.ingredientsData);
                res.status(201).json(shop);
            } catch (e) {
                res.status(406).end();
            }
    });

    app.get('/shops', async (req, res) => {
        try {
            const shop = await ShopController.findAllShops();
            res.status(202).json(shop);
        } catch (e) {
            res.status(409).end();
        }
    });

    app.get('/shop/:id',ShopValidator.checkId(), validate, async (req, res) => {
        try {
            const id = parseInt(req.params.id);
            const shop = await ShopController.findShopById(id);
            res.status(202).json(shop);
        } catch (e) {
            res.status(409).end();
        }
    });

    app.get('/shop/:id/stocks',ShopValidator.checkId(), validate, async (req, res) => {
        try {
            const id = parseInt(req.params.id);
            const shop = await ShopController.findStocksOfShop(id);
            res.status(202).json(shop);
        } catch (e) {
            res.status(409).end();
        }
    });

    app.delete('/shop/:id', UserMiddleware.checkIfAuthIsAdmin(),
        ShopValidator.checkId(), validate, async(req, res) => {
        try {
            const id = parseInt(req.params.id);
            await ShopController.deleteShop(id);
            res.status(204).end();
        } catch (e) {
            res.status(404).end();
        }
    });

    app.delete('/shop/:id/stock/:ingredientId', UserMiddleware.checkIfAuthIsAdmin(),
        ShopValidator.deleteStockRules(), validate, async(req, res) => {
        try {
            const id = parseInt(req.params.id);
            await ShopController.deleteStock(id, req.params.ingredientId);
            res.status(204).end();
        } catch (e) {
            res.status(404).end();
        }
    });

    app.put('/shop/:id', UserMiddleware.checkIfAuthIsAdmin(), bodyParser.json(),
        ShopValidator.updateShopRules(), validate, async (req, res) => {
        try {
            const id = parseInt(req.params.id);
            const shop = await ShopController.updateShop(id, req.body.name, req.body.address, req.body.ingredientsData);
            res.status(201).json(shop);
        } catch (e) {
            res.status(406).end();
        }
    });


};
