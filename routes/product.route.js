const bodyParser = require('body-parser');
const ProductController = require('../controllers').ProductController;
const ProductValidator = require('../validators').ProductValidator;
const middleware = require('../middlewares');
const validate = middleware.validate;
const UserMiddleware = middleware.UserMiddleware;

module.exports = function(app) {

    app.post('/product', UserMiddleware.checkIfAuthIsAdmin(), bodyParser.json(), ProductValidator.createProductValidationRules(), validate, async (req, res) => {
            try {
                const product = await ProductController.createProduct(req.body.name, req.body.price, req.body.ingredientsData);
                res.status(201).json(product);
            } catch (e) {
                res.status(406).end();
            }
    });

    app.post('/product/:id/ingredient', UserMiddleware.checkIfAuthIsAdmin(), bodyParser.json(), ProductValidator.addProductIngredientValidationRules(), validate, async (req, res) => {
            try {
                const product = await ProductController.addProductIngredient(req.params.id, req.body.ingredientsData);
                res.status(201).json(product);
            } catch (e) {
                res.status(406).end();
            }
    });

    app.get('/product/:id', ProductValidator.checkId(), validate, async (req, res) => {
        try {
            const product = await ProductController.findProductById(req.params.id);
            res.status(202).json(product);
        } catch (e) {
            res.status(404).end();
        }
    });

    app.get('/products', async (req, res) => {
        try {
            const products = await ProductController.findAllProducts();
            res.status(201).json(products);
        } catch (e) {
            res.status(409).end();
        }
    });

    app.delete('/product/:id', UserMiddleware.checkIfAuthIsAdmin(), ProductValidator.checkId(), validate, async (req, res) => {
        try {
            await ProductController.deleteProduct(req.params.id);
            res.status(204).end();
        } catch (e) {
            res.status(404).end();
        }
    });

    app.delete('/product/:productId/ingredient/:ingredientId', UserMiddleware.checkIfAuthIsAdmin(), ProductValidator.deleteProductIngredientValidationRules(), validate, async (req, res) => {
        try {
            await ProductController.deleteProductIngredient(req.params.productId, req.params.ingredientId);
            res.status(204).end();
        } catch (e) {
            res.status(404).end();
        }
    });

    app.put('/product/:id', UserMiddleware.checkIfAuthIsAdmin(),
        bodyParser.json(),
        ProductValidator.updateProductValidationRules(),
        validate, async (req, res) => {
            try {
                const id = parseInt(req.params.id);
                const product = await ProductController.updateProduct(id, req.body.name, req.body.price, req.body.ingredientsData);
                res.status(201).json(product);
            } catch (e) {
                res.status(406).end();
            }
    });


};
