const bodyParser = require('body-parser');
const IngredientController = require('../controllers').IngredientController;
const IngredientValidator = require('../validators').IngredientValidator;
const middleware = require('../middlewares');
const validate = middleware.validate;
const UserMiddleware = middleware.UserMiddleware;

module.exports = function(app) {

    app.post('/ingredient', UserMiddleware.checkIfAuthIsAdmin(),
        bodyParser.json(), IngredientValidator.createIngredientValidationRules(),
        validate, async (req, res) => {
            try {
                const ingredient = await IngredientController.createIngredient(req.body.name);
                res.status(201).json(ingredient);
            } catch (e) {
                res.status(406).end();
            }
    });

    app.get('/ingredients', async (req, res) => {
        try {
            const ingredient = await IngredientController.findAllIngredients();
            res.status(202).json(ingredient);
        } catch (e) {
            res.status(404).end();
        }
    });

    app.get('/ingredient/:id', IngredientValidator.checkId(), validate, async (req, res) => {
        try {
            const id = parseInt(req.params.id);
            const ingredient = await IngredientController.findIngredientById(id);
            res.status(202).json(ingredient);
        } catch (e) {
            res.status(404).end();
        }
    });

    app.delete('/ingredient/:id', UserMiddleware.checkIfAuthIsAdmin(),
        IngredientValidator.checkId(), validate, async(req, res) => {
        try {
            const id = parseInt(req.params.id);
            await IngredientController.deleteIngredient(id);
            res.status(204).end();
        } catch (e) {
            res.status(404).end();
        }
    });

    app.put('/ingredient/:id', UserMiddleware.checkIfAuthIsAdmin(),
        bodyParser.json(), IngredientValidator.updateIngredientValidateRules(),
        validate, async (req, res) => {
        try {
            const id = parseInt(req.params.id);
            const ingredient = await IngredientController.updateIngredient(id, req.body.name);
            res.status(201).json(ingredient);
        } catch (e) {
            res.status(406).end();
        }
    });


};